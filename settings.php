<?php 
global $wpdb, $q_config;
$prefix=$wpdb->base_prefix;
$error=array();


if(isset($_POST['registration']))
{
	
	$count2=0;
	$client_confirmation_message='';
	$sales_confirmation_message='';
	$client_confirmation_subject='';
	$sales_confirmation_subject='';
	foreach ( qtrans_getSortedLanguages() as $key => $language ) {
		$client_confirmation_message.='<!--:'.$language.'-->'.$_POST['client_confirmation_message_'.$count2].'<!--:-->';
		$sales_confirmation_message.='<!--:'.$language.'-->'.$_POST['sales_confirmation_message_'.$count2].'<!--:-->';
		$client_confirmation_subject.='<!--:'.$language.'-->'.$_POST['client_confirmation_subject_'.$count2].'<!--:-->';
		$sales_confirmation_subject.='<!--:'.$language.'-->'.$_POST['sales_confirmation_subject_'.$count2].'<!--:-->';
		$count2++;
	}
	
	
	$shopsettings=shop_settings(" and setting_key='client_confirmation_subject'");
	if(count($shopsettings)>0)
	{
		$sql="UPDATE `".$prefix."shop_settings` set setting_value='$client_confirmation_subject' where setting_key='client_confirmation_subject'";
	}
	else
	{
		$sql="INSERT INTO `".$prefix."shop_settings` (`setting_key`,`setting_value`) VALUES ('client_confirmation_subject', '$client_confirmation_subject')";
	}
	$result = $wpdb->query( $sql );
	
	$shopsettings=shop_settings(" and setting_key='client_confirmation_message'");
	if(count($shopsettings)>0)
	{
		$sql="UPDATE `".$prefix."shop_settings` set setting_value='$client_confirmation_message' where setting_key='client_confirmation_message'";
	}
	else
	{
		$sql="INSERT INTO `".$prefix."shop_settings` (`setting_key`,`setting_value`) VALUES ('client_confirmation_message', '$client_confirmation_message')";
	}
	$result = $wpdb->query( $sql );
	
	$shopsettings=shop_settings(" and setting_key='sales_confirmation_subject'");
	if(count($shopsettings)>0)
	{
		$sql="UPDATE `".$prefix."shop_settings` set setting_value='$sales_confirmation_subject' where setting_key='sales_confirmation_subject'";
	}
	else
	{
		$sql="INSERT INTO `".$prefix."shop_settings` (`setting_key`,`setting_value`) VALUES ('sales_confirmation_subject', '$sales_confirmation_subject')";
	}
	$result = $wpdb->query( $sql );
	
	$shopsettings=shop_settings(" and setting_key='sales_confirmation_message'");
	if(count($shopsettings)>0)
	{
		$spec_id=$category_specifications[0]->id;
		$sql="UPDATE `".$prefix."shop_settings` set setting_value='$sales_confirmation_message' where setting_key='sales_confirmation_message'";
	}
	else
	{
		$sql="INSERT INTO `".$prefix."shop_settings` (`setting_key`,`setting_value`) VALUES ('sales_confirmation_message', '$sales_confirmation_message')";
	}
	$result = $wpdb->query( $sql );
	
		
	$url=get_option('home').'/wp-admin/admin.php?page=Settings&update=succ';
	echo"<script>window.location='".$url."'</script>";
		
}

$shopsettings=shop_settings();
if(count($shopsettings)>0)
{
	foreach($shopsettings as $shopsetting)
	{
		if($shopsetting->setting_key=='client_confirmation_message')
		{
			$client_confirmation_message=$shopsetting->setting_value;
		}
		if($shopsetting->setting_key=='sales_confirmation_message')
		{
			$sales_confirmation_message=$shopsetting->setting_value;
		}
		if($shopsetting->setting_key=='client_confirmation_subject')
		{
			$client_confirmation_subject=$shopsetting->setting_value;
		}
		if($shopsetting->setting_key=='sales_confirmation_subject')
		{
			$sales_confirmation_subject=$shopsetting->setting_value;
		}
		
	}
}

?>
<style type="text/css">
.error
{
	color:#CC0000;
}
.donotshowerror label.error
{
	display: none !important;
}
label.error
{
	margin-left:10px;
}
input.error, select.error,textarea.error, checkbox.error
{
	color:#000000;
	border:1px solid #CC0000 !important;
}
input[type='checkbox'].error
{
	border: solid #CC0000;
	outline:1px solid #CC0000 !important;
}
.personal_info{float:left; width:160px;}
.e-mail{ clear:both;}
.adress{ width:168px; float:left; text-align:left; font-size:13px; color:#454546;}
.field{ float:left; width:600px;}
.field input, .field select{ width:324px; height:30px; padding:0 !important; border:1px solid #c7cecf;  border:1px solid #c7cecf; margin:0px 0px 10px 0; background:#f0f0f0; }
.field textarea{ width:500px; padding:0 !important; border:1px solid #c7cecf;  border:1px solid #c7cecf; margin:0px 0px 10px 0; background:#f0f0f0; }
.profile .green-submit-btn input[type="submit"], .profile .green-submit-btn input[type="button"]{ width:152px; border:1px solid #b4babb; height: 45px; line-height:45px; text-align:center; color:#000; font-size:17px; font-weight:bold; border-radius:5px; display:block; font-family:Arial, Helvetica, sans-serif; cursor:pointer; }
.profile .green-submit-btn input[type="button"]{ margin-left:20px;}
.field .wp-core-ui input, .field .wp-core-ui select{ width:auto; height:auto;}
input, select, textarea{float:left;}
.clr{clear:both; margin-top:10px;}.mr5{margin-right:5px;}
.fl{float:left;}.removeday, .addday{float:left; color:#FF0000; font-size:18px; text-decoration:none; margin-left:10px;}.addday{color:#0000FF;}
.tt{float:left; width:70px;}
.sparator{width:600px; margin:5px 0px; height:1px; border-bottom:1px solid #000000;} 
.ml10{margin-left:10px;}
.mt10{margin-top:10px;}
.remove{margin-left:10px; margin-top:10px;}
.tabs{float:left; margin-bottom:10px; width:90%; border-bottom:1px solid #000000;}
.tabs a{float:left; padding:5px 5px; margin-right:1px; font-size:14px; color:#666666; background-color:#9999FF; text-decoration:none; outline:none;}
.tabs a.active{ color:#000000; background-color:#fff;}
.destinationprice table{border-bottom: 1px solid #000; padding:5px 0px;}
</style>
<script type="text/javascript" src="<?php echo get_option('home');?>/wp-content/plugins/products/js/jquery.js"></script>
<script type="text/javascript" src="<?php echo get_option('home');?>/wp-content/plugins/products/js/validate.js"></script>
<script type="text/javascript">
jQuery(document).ready(function(){
	jQuery("#register_spcialist").validate();
	
	var intv=setInterval( function(){
		jQuery('.mceLayout').css('height','300px');
		jQuery('.wp-editor-area').css('height','200px');
		jQuery('.mceLayout iframe').css('height','200px');
		clearInterval(intv);
	},2000);
	jQuery('.rightpage').hide();
	jQuery('.leftpage').show();
	jQuery('.gensettings').addClass('active');
	jQuery('.gensettings').live('click', function(){
		jQuery('.rightpage').hide();
		jQuery('.leftpage').show();
		jQuery('.tabs a').removeClass('active');
		jQuery(this).addClass('active');
	});
	jQuery('.tempsettings').live('click', function(){
		jQuery('.leftpage').hide();
		jQuery('.rightpage').show();
		jQuery('.tabs a').removeClass('active');
		jQuery(this).addClass('active');
		
	});
	
});
</script>
<h2>Settings</h2>
<div class="clr"></div>
<?php if(isset($_REQUEST['del'])){if($_REQUEST['del']=='succ'){ ?>
	<div class="updated"><p><strong><?php _e('Deleted successfully.' ); ?></strong></p></div>
<?php }} ?>
<?php if(isset($_REQUEST['add'])){if($_REQUEST['add']=='succ'){ ?>
	<div class="updated"><p><strong><?php _e('Added successfully.' ); ?></strong></p></div>
<?php }} ?>
<?php if(isset($_REQUEST['update'])){if($_REQUEST['update']=='succ'){ ?>
	<div class="updated"><p><strong><?php _e('Update successfully.' ); ?></strong></p></div>
<?php }} ?>
<div class="clr"></div>
	<div class="tabs">
    	<a href="javascript:;" class="gensettings">Email Templates</a>
        <!--<a href="javascript:;" class="tempsettings">Category Specifications</a>-->
    </div>
	<div class="profile donotshowerror">
    	<?php if(count($error)>0)
		  { ?>
		<div class="tabletitle"><span class="error">Error</span></div>
		<table width="700" class="from_main" border="0" cellpadding="0" cellspacing="0">
		  <?php 
		   
			for($i=0;$i<count($error);$i++)
			{
				?>
			  <tr>
				<td align="left" valign="top" class="name"><span class="error"><?php echo $error[$i]; ?></span></td>
			</tr>
	<?php	} ?>
		</table>
		<div class="clr mt20"></div>
	 <?php } ?>
        	<form action="" method="post" name="register_spcialist" id="register_spcialist" enctype="multipart/form-data">
            	<div class="clr"></div
            	<p><strong>Please do not change the constant word in between the %%</strong></p>
                <p><strong>%request_number%</strong> => order number</p>
                <p><strong>%name%</strong> => Name of client.</p>
                <p><strong>%product_list%</strong> => List of products</p>
                <p><strong>%sales_name%</strong> => Name of sales</p>
                <p><strong>%ref%</strong> => Reference number</p>
                <div class="clr"></div>
				<div class="leftpage">
            	<?php	$count=0; foreach ( qtrans_getSortedLanguages() as $key => $language ) { ?>
                <div class="e-mail">
                    <div class="adress">Client's Order conf. subject in <?php echo $q_config['language_name'][$language]; ?>:  </div>
                    <div class="field"><input type="text" name="client_confirmation_subject_<?php echo $count; ?>" value="<?php _e(getText22($client_confirmation_subject,$language)); ?>" /></div>
                </div>
                <div class="clr"></div>
                <div class="e-mail" style="width:900px; height:200px;">
                    <div class="adress">Client's Order conf. message in <?php echo $q_config['language_name'][$language]; ?>:  </div>
                    <div class="field"><?php the_editor(getText22($client_confirmation_message,$language), 'client_confirmation_message_'.$count); ?></div>
                </div>
                <div class="clr mt10">&nbsp;</div>
                
                <div class="e-mail">
                    <div class="adress">Sales man's Order conf. subject in <?php echo $q_config['language_name'][$language]; ?>:  </div>
                    <div class="field"><input type="text" name="sales_confirmation_subject_<?php echo $count; ?>" value="<?php _e(getText22($sales_confirmation_subject,$language)); ?>" /></div>
                </div>
                <div class="clr"></div>
                <div class="e-mail" style="width:900px; height:200px;">
                    <div class="adress">Sales man's Order conf. message in <?php echo $q_config['language_name'][$language]; ?>:  </div>
                    <div class="field"><?php the_editor(getText22($sales_confirmation_message,$language), 'sales_confirmation_message_'.$count); ?></div>
                </div>
                <div class="clr mt10">&nbsp;</div>
                <?php $count++;} ?>
                
                <div class="clr"></div>
                
                </div>
            	<div class="clr"></div>
                <div class="e-mail">
                    <div class="adress">&nbsp;&nbsp;</div>
                    <div class="field" style="margin-top:10px;">
                        <div class="green-submit-btn">
                        	<input type="submit" name="registration" value="SUBMIT" class="registration_btn"/>
                         </div>
                    </div>
                </div>
                
            </form>
        
      </div>
<div class="clr"></div>