<?php 
global $wpdb, $q_config;
$prefix=$wpdb->base_prefix;
$error=array();
$showrcords=1;
$parent=0;
if(isset($_POST['registration']) || isset($_POST['registration2']))
{
	
	$types=$_POST['types'];
	$showin_filters=$_POST['showin_filters'];
	$showrcords=count($types);
	$orderby=$_POST['orderby'];
	
	$count2=0;
	$title='';
	$desctiption='';
	$specifications=array();
	foreach ( qtrans_getSortedLanguages() as $key => $language ) {
		$title.='<!--:'.$language.'-->'.$_POST['title_'.$count2].'<!--:-->';
		$desctiption.='<!--:'.$language.'-->'.$_POST['desctiption_'.$count2].'<!--:-->';
		for($i=0;$i<=$showrcords;$i++)
		{
			$specifications[$i].='<!--:'.$language.'-->'.$_POST['specifications_'.$count2][$i].'<!--:-->';
		}
		$count2++;
	}
	//echo $specifications;
	
	if(trim($title)=='')
	{
		array_push($error,'Please enter name.');
	}
	if(trim($desctiption)=='')
	{
		array_push($error,'Please enter desctiption.');
	}
	$parent=$_POST['parent'];
	if(count($error)<=0)
	{
		
		$alias=$_POST['title_0'];
				
		$order   = array("&","$","%","!","~","'","`","_","(",")","{","}","[","]",":",";","<",">",",",".","/","|","=","+","*","#","-"," ","  ","   ","�");
		$replace = '';
		$alias=strtolower(str_replace($order, $replace, $alias));
		
		$check=product_categories('', " and alias='$alias'");
		if(count($check)>0)
		{
			$alias=$alias.'-2';
			$check=product_categories('', " and alias='$alias'");
			if(count($check)>0)
			{
				$alias=$alias.'-3';
				$check=product_categories('', " and alias='$alias'");
				if(count($check)>0)
				{
					$alias=$alias.'-4';
				}
			}
		}
	
		$sql="INSERT INTO `".$prefix."product_category` (`name`, `description`, `add_date`, `parent`, `alias`) VALUES ('$title', '$desctiption', now(), '$parent', '$alias')";
		$result = $wpdb->query( $sql );
		if($result==1)
		{
			$lastid=$wpdb->insert_id;
			for($i=0;$i<=$showrcords;$i++)
			{
				$orderby=$_POST['orderby'][$i];
				$type=$types[$i];
				$showin_filter=$showin_filters[$i];
				$option_key=$_POST['specifications_0'][$i];
				
				$order   = array("&","$","%","!","~","'","`","_","(",")","{","}","[","]",":",";","<",">",",",".","/","|","=","+","*","#","-"," ","  ","   ");
				$replace = '';
				$option_key=strtolower(str_replace($order, $replace, $option_key));
				
				$specification='';
				$count2=0;
				foreach ( qtrans_getSortedLanguages() as $key => $language ) {
					
					$specification.='<!--:'.$language.'-->'.$_POST['specifications_'.$count2][$i].'<!--:-->';
					$count2++;
				}
				
				if(trim($type)!='' && trim($option_key)!='')
				{
					$sql="INSERT INTO `".$prefix."category_specifications` (`category_id`,`option_key`, `option_value`,`field_type`, `add_date`, `showin_filter`, `orderby`) VALUES ('$lastid', '$option_key', '$specification', '$type', now(), '$showin_filter', '$orderby')";
					$result = $wpdb->query( $sql );
				}
			}
			
			$sql="INSERT INTO `".$prefix."terms` (`name`, `slug`) VALUES ('$title', '$alias')";
			$result2 = $wpdb->query( $sql );
			if($result2==1)
			{
				$term_id=$wpdb->insert_id;
				
				$tpatent=0;
				$check2=product_categories($parent);
				if(count($check2)>0)
				{
					$tid=$check2[0]->term_id;
					$terms=category_terms(" and term_id='$tid' and taxonomy='product_category'");
					if(count($terms)>0)
					{
						$tpatent=$check2[0]->term_id;
					}
				}
				
				$sql="INSERT INTO `".$prefix."term_taxonomy` (`term_id`, `taxonomy`, `description`, `parent`) VALUES ('$term_id', 'product_category','$desctiption','$tpatent')";
				$result2 = $wpdb->query( $sql );
				
				$sql="UPDATE `".$prefix."product_category` set term_id='$term_id' where id='$lastid'";
				$result = $wpdb->query( $sql );
			}
			
			$file='image';
			if(isset($_FILES[$file]['name']))
			{
					if($_FILES[$file]['name']!='')
					{
						if ( (strtolower($_FILES[$file]["type"]) == "image/gif")
					|| (strtolower($_FILES[$file]["type"]) == "image/jpeg")
					|| (strtolower($_FILES[$file]["type"]) == "image/jpg")
					|| (strtolower($_FILES[$file]["type"]) == "image/png")
					|| (strtolower($_FILES[$file]["type"]) == "image/pjpeg"))
					  {
						if ($_FILES[$file]["error"] > 0)
						{
							 echo "Error: " . $_FILES[$file]["error"] . "<br />";
						}
						else
						{
							if (!is_dir('../wp-content/uploads/products/cat')) {
								mkdir('../wp-content/uploads/products/cat');
							}
							
							$exts=explode('.',$_FILES[$file]["name"]);
							$exten='.'.$exts[count($exts)-1];
							$altername=$alias.'-'.$lastid.$exten;
							  move_uploaded_file($_FILES[$file]["tmp_name"], "../wp-content/uploads/products/cat/" . $_FILES[$file]["name"]);
							  rename("../wp-content/uploads/products/cat/".$_FILES[$file]["name"], "../wp-content/uploads/products/cat/$altername");
							$sql="UPDATE `".$prefix."product_category` set image='$altername' where id='$lastid'";
							$result = $wpdb->query( $sql );
						}
					}
					
				}
			}
			
			if(isset($_POST['registration2']))
			{
				$url=get_option('home').'/wp-admin/admin.php?page=Categories&usr=editcategory&add=succ&id='.$lastid;
			}
			else
			{
				$url=get_option('home').'/wp-admin/admin.php?page=Categories&add=succ';
			}
			echo"<script>window.location='".$url."'</script>";
		}
	}
}

?>
<style type="text/css">
.error
{
	color:#CC0000;
}
.donotshowerror label.error
{
	display: none !important;
}
label.error
{
	margin-left:10px;
}
input.error, select.error,textarea.error, checkbox.error
{
	color:#000000;
	border:1px solid #CC0000 !important;
}
input[type='checkbox'].error
{
	border: solid #CC0000;
	outline:1px solid #CC0000 !important;
}
.personal_info{float:left; width:160px;}
.e-mail{ clear:both;}
.adress{ width:168px; float:left; text-align:left; font-size:13px; color:#454546;}
.field{ float:left; width:600px;}
.field input, .field select{ width:324px; height:30px; padding:0 !important; border:1px solid #c7cecf;  border:1px solid #c7cecf; margin:0px 0px 10px 0; background:#f0f0f0; }
.field textarea{ width:500px; padding:0 !important; border:1px solid #c7cecf;  border:1px solid #c7cecf; margin:0px 0px 10px 0; background:#f0f0f0; }
.profile .green-submit-btn input[type="submit"], .profile .green-submit-btn input[type="button"]{ width:152px; border:1px solid #b4babb; height: 45px; line-height:45px; text-align:center; color:#000; font-size:17px; font-weight:bold; border-radius:5px; display:block; font-family:Arial, Helvetica, sans-serif; cursor:pointer; }
.profile .green-submit-btn input[type="button"]{ margin-left:20px;}
.field .wp-core-ui input, .field .wp-core-ui select{ width:auto; height:auto;}
input, select, textarea{float:left;}
.clr{clear:both; margin-top:10px;}.mr5{margin-right:5px;}
.fl{float:left;}.removeday, .addday{float:left; color:#FF0000; font-size:18px; text-decoration:none; margin-left:10px;}.addday{color:#0000FF;}
.tt{float:left; width:70px;}
.sparator{width:600px; margin:5px 0px; height:1px; border-bottom:1px solid #000000;} 
.ml10{margin-left:10px;}
.mt10{margin-top:10px;}
.remove{margin-left:10px; margin-top:10px;}
.tabs{float:left; margin-bottom:10px; width:90%; border-bottom:1px solid #000000;}
.tabs a{float:left; padding:5px 5px; margin-right:1px; font-size:14px; color:#666666; background-color:#9999FF; text-decoration:none; outline:none;}
.tabs a.active{ color:#000000; background-color:#fff;}
.destinationprice table{border-bottom: 1px solid #000; padding:5px 0px;}
</style>
<script type="text/javascript" src="<?php echo get_option('home');?>/wp-content/plugins/products/js/jquery.js"></script>
<script type="text/javascript" src="<?php echo get_option('home');?>/wp-content/plugins/products/js/validate.js"></script>
<script type="text/javascript">
jQuery(document).ready(function(){
	jQuery("#register_spcialist").validate();
	<?php if(trim($showrcords)>0){  ?>
		var llang='';
		
		<?php  
			for($i=0;$i<$showrcords;$i++){?>
			
			llang+='<table width="700" border="0" cellpadding="3" cellspacing="3" class="table_<?php _e($i); ?> fl"><?php	$count=0; foreach ( qtrans_getSortedLanguages() as $key => $language ) { ?><tr><td align="left" valign="top">Specification in <?php echo $q_config['language_name'][$language]; ?></td><td align="left" valign="top"><input type="text" name="specifications_<?php echo $count; ?>[]" value="<?php _e(getText22($specifications[$i],$language)); ?>" /></td><?php if($count==0){ ?><td align="left" valign="top" rowspan="2">Choose Field Type<br /><select name="types[]"><option value="text"<?php if($types[$i]=='text'){_e(' selected="selected"');} ?>>Text</option><option value="checkbox"<?php if($types[$i]=='checkbox'){_e(' selected="selected"');} ?>>Checkbox</option><option value="degree"<?php if($types[$i]=='degree'){_e(' selected="selected"');} ?>>Angle</option><option value="number"<?php if($types[$i]=='number'){_e(' selected="selected"');} ?>>Number</option></select></td><td align="left" valign="top" rowspan="2">Show in filter<br /><select name="showin_filters[]"><option value="N"<?php if($showin_filters[$i]=='N'){_e(' selected="selected"');} ?>>No</option><option value="Y"<?php if($showin_filters[$i]=='Y'){_e(' selected="selected"');} ?>>Yes</option></select></td><td align="left" valign="top" rowspan="2">Order By<br /><input type="text" name="orderby[]" size="5" class="number" value="<?php _e($orderby[$i]); ?>" /></td><?php } ?></tr><?php $count++;} ?></table><a href="javascript:;" class="remove fl" coords="<?php _e($i); ?>">Remove</a>';
			<?php } ?>
			
			jQuery('.destinationprice').html(llang);
			
	<?php } ?>
	
	jQuery('.addmore').live('click', function(){
		var lasttable=jQuery('.destinationprice table:last').attr('class');
		var nexttable=lasttable.split('_');
		totaltable=parseInt(nexttable[1])+1;
		
			llang='<table width="700" border="0" cellpadding="3" cellspacing="3" class="table_'+totaltable+' fl"><?php	$count=0; foreach ( qtrans_getSortedLanguages() as $key => $language ) { ?><tr><td align="left" valign="top">Specification in <?php echo $q_config['language_name'][$language]; ?></td><td align="left" valign="top"><input type="text" name="specifications_<?php echo $count; ?>[]" value="" /></td><?php if($count==0){ ?><td align="left" valign="top" rowspan="2">Choose Field Type<br /><select name="types[]"><option value="text">Text</option><option value="checkbox">Checkbox</option><option value="degree">Angle</option><option value="number">Number</option></select></td><td align="left" valign="top" rowspan="2">Show in filter<br /><select name="showin_filters[]"><option value="N">No</option><option value="Y">Yes</option></select></td><td align="left" valign="top" rowspan="2">Order By<br /><input type="text" name="orderby[]" size="5" class="number" value="0" /></td><?php } ?></tr><?php $count++;} ?></table><a href="javascript:;" class="remove fl" coords="'+totaltable+'">Remove</a>';
		
		jQuery('.destinationprice').append(llang);
		
	});
	
	jQuery('.remove').live('click', function(){
		var tableid=jQuery(this).attr('coords');
		jQuery('.table_'+tableid).remove();	
		jQuery(this).remove();	
	});
	
	var intv=setInterval( function(){
		jQuery('.mceLayout').css('height','300px');
		jQuery('.wp-editor-area').css('height','200px');
		jQuery('.mceLayout iframe').css('height','200px');
		clearInterval(intv);
	},2000);
	
	jQuery('.rightpage').hide();
	jQuery('.leftpage').show();
	jQuery('.gensettings').addClass('active');
	jQuery('.gensettings').live('click', function(){
		jQuery('.rightpage').hide();
		jQuery('.leftpage').show();
		jQuery('.tabs a').removeClass('active');
		jQuery(this).addClass('active');
	});
	jQuery('.tempsettings').live('click', function(){
		jQuery('.leftpage').hide();
		jQuery('.rightpage').show();
		jQuery('.tabs a').removeClass('active');
		jQuery(this).addClass('active');
		
	});
});
</script>
<h2>Add Category</h2>
	<div class="tabs">
    	<a href="javascript:;" class="gensettings">Category Detail</a>
        <a href="javascript:;" class="tempsettings">Category Specifications</a>
    </div>
	<div class="profile donotshowerror">
    	<?php if(count($error)>0)
		  { ?>
		<div class="tabletitle"><span class="error">Error</span></div>
		<table width="700" class="from_main" border="0" cellpadding="0" cellspacing="0">
		  <?php 
		   
			for($i=0;$i<count($error);$i++)
			{
				?>
			  <tr>
				<td align="left" valign="top" class="name"><span class="error"><?php echo $error[$i]; ?></span></td>
			</tr>
	<?php	} ?>
		</table>
		<div class="clr mt20"></div>
	 <?php } ?>
        
        	<form action="" method="post" name="register_spcialist" id="register_spcialist" enctype="multipart/form-data">
            <div class="leftpage">
            	<div class="e-mail">
                    <div class="adress">Parent:  </div>
                    <div class="field">
                    	<select name="parent" id="wallpaper">
                            <option value="0">Select Parent Category</option>
                            <?php list_all($id,$parent,'en');?>
                        </select>
                    </div>
                </div>
            	<?php	$count=0; foreach ( qtrans_getSortedLanguages() as $key => $language ) { ?>
                <div class="e-mail">
                    <div class="adress">Name in <?php echo $q_config['language_name'][$language]; ?>:  </div>
                    <div class="field"><input type="text" name="title_<?php echo $count; ?>" value="<?php _e(getText22($title,$language)); ?>" /></div>
                </div>
                <div class="clr"></div>
                <div class="e-mail" style="width:900px; height:200px;">
                    <div class="adress">Detail in <?php echo $q_config['language_name'][$language]; ?>:  </div>
                    <div class="field"><?php the_editor(getText22($desctiption,$language), 'desctiption_'.$count); ?></div>
                </div>
                <div class="clr mt10">&nbsp;</div>
                <?php $count++;} ?>
                <div class="e-mail">
                    <div class="adress">Image:  </div>
                    <div class="field"><input type="file" name="image" /></div>
                </div>
                <div class="clr"></div>
                </div>
                <div class="rightpage">
                	<div class="clr"></div>
                	<div class="destinationprice"></div>
                    <div class="clr"></div>
                	<a href="javascript:;" class="addmore">Add New</a>
                </div>
                
            	<div class="clr"></div>
                
                <div class="e-mail">
                    <div class="adress">&nbsp;&nbsp;</div>
                    <div class="field" style="margin-top:10px;">
                        <div class="green-submit-btn">
                        	<input type="submit" name="registration" value="SUBMIT" class="registration_btn"/> <input style="margin-left:20px;" type="submit" name="registration2" value="Save and Edit" class="registration_btn"/> <input onclick="return backtolist()" type="button" name="back" value="Back" title="Back" />
                       
                         </div>
                    </div>
                </div>
                
            </form>
        
        </div>
<div class="clr"></div>

<script type="text/javascript">
function backtolist()
{
	window.location='<?php echo get_option('home').'/wp-admin/admin.php?page=Categories'; ?>';
}
</script>
<div class="clr"></div>