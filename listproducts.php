<?php if(!isset($_REQUEST['usr']) )
{ 
	global $wpdb,$signature;
	$prefix=$wpdb->base_prefix;
	$blog_id = $wpdb->blogid;
	
	$urlink='';
	$showtotal=10;
	if(isset($_REQUEST['showtotal']) && trim($_REQUEST['showtotal'])!='' && trim($_REQUEST['showtotal'])!='0')
	{
		$showtotal=$_REQUEST['showtotal'];
		$urlink.='&showtotal='.$showtotal;
	}
	
	$totalrec=$showtotal;
	if(isset($_REQUEST['pagedid']) && $_REQUEST['pagedid']>1)
	{
		$pageid=$_REQUEST['pagedid'];
		$limitstart=$totalrec*($pageid-1);
	}
	else
	{
		$pageid=1;
		$limitstart=0;
		$limitsend=$totalrec;
	}
	
	$cond='';
	
	
	
	if(isset($_REQUEST['category']) && trim($_REQUEST['category'])!='' && trim($_REQUEST['category'])!='0')
	{
		$category=$_REQUEST['category'];
		$cond.=" and category_id='$category'";
		$urlink.='&category='.$category;
	}
	
	if(isset($_REQUEST['txt']) && trim($_REQUEST['txt'])!='')
	{
		$txt=$_REQUEST['txt'];
		$txt1=strtolower($txt);
		$cond.=" and ( lower(product_name) like '%$txt1%' || lower(product_number) like '%$txt1%')";
		$urlink.='&txt='.$txt;
	}
	
	$where=" where id!='' $cond order by orderby asc";
	$querystr = "SELECT * FROM ".$prefix."products $where limit $limitstart, $totalrec";
	$trips = $wpdb->get_results($querystr, OBJECT);
	
	$querystr = "SELECT * FROM ".$prefix."products $where";
	$totalphotos = $wpdb->get_results($querystr, OBJECT);
?>
<style type="text/css">
table td,table th{padding:5px;}
.pagination{ float:left; line-height:30px; font-size:14px; font-weight:bold;}
.pagination span{background:#f6f6f6; color:#000; padding:0px 10px; text-decoration:underline;}
.pagination a{background:#FFFFFF color:#0000FF; padding:0px 10px; text-decoration:none;}
.pagination a:hover{text-decoration:underline;}
ul.config{	padding:10px;	margin:0px;}
ul.config li{	display:inline;	float:left;	padding:0px 10px;}
ul.config li a{	text-decoration:none;	color:#000066;}
ul.config li a:hover, ul.config li a.active{	text-decoration:underline;	color:#990000;}
.clr{clear:both;}
.fl{float:left;}
.fr{float:right;}
.orderby{position:relative;}
.load{position:absolute; top:6px; left:6px; display:none;}
</style>
<?php $url=get_option('home').'/wp-admin/admin.php?page=Products'; ?>
<div class="wrap">
<?php    echo "<h2>" . __( 'Manage Products', 'webserve_trdom' ) . "</h2>"; ?>

<div class="clr"></div>
<?php if(isset($_REQUEST['del'])){if($_REQUEST['del']=='succ'){ ?>
	<div class="updated"><p><strong><?php _e('Deleted successfully.' ); ?></strong></p></div>
<?php }} ?>
<?php if(isset($_REQUEST['add'])){if($_REQUEST['add']=='succ'){ ?>
	<div class="updated"><p><strong><?php _e('Added successfully.' ); ?></strong></p></div>
<?php }} ?>
<?php if(isset($_REQUEST['update'])){if($_REQUEST['update']=='succ'){ ?>
	<div class="updated"><p><strong><?php _e('Update successfully.' ); ?></strong></p></div>
<?php }} ?>
<div class="clr"></div>
<script type="text/javascript" src="<?php echo get_option('home');?>/wp-content/plugins/products/js/jquery.js"></script>
<script type="text/javascript">
	jQuery(document).ready( function(){
		jQuery('.newproduct a').live('click', function(){
			var vars=jQuery(this).attr('coords');
			var varibles=vars.split(',');
			var id=varibles[0];
			var field=varibles[1];
			var value=varibles[2];
		
			jQuery.post("<?php echo get_option('home');?>/wp-content/plugins/products/updateoptions.php", { id: id, field: field, value: value},
				function(data) {
					jQuery('.newproduct_'+id).html(data);
				});
		});
		
		jQuery('.special a').live('click', function(){
			var vars=jQuery(this).attr('coords');
			var varibles=vars.split(',');
			var id=varibles[0];
			var field=varibles[1];
			var value=varibles[2];
		
			jQuery.post("<?php echo get_option('home');?>/wp-content/plugins/products/updateoptions.php", { id: id, field: field, value: value},
				function(data) {
					jQuery('.special_'+id).html(data);
				});
		});
		
		jQuery('.builderoffers a').live('click', function(){
			var vars=jQuery(this).attr('coords');
			var varibles=vars.split(',');
			var id=varibles[0];
			var field=varibles[1];
			var value=varibles[2];
		
			jQuery.post("<?php echo get_option('home');?>/wp-content/plugins/products/updateoptions.php", { id: id, field: field, value: value},
				function(data) {
					jQuery('.builderoffers_'+id).html(data);
				});
		});
		
		jQuery('.orderby input').live('keyup', function(){
			var vars=jQuery(this).attr('id');
			var varibles=vars.split('_');
			var id=varibles[1];
			var value=jQuery(this).val();
			
			if(parseInt(value)>=0)
			{
				jQuery('.load_'+id).show();
				jQuery.post("<?php echo get_option('home');?>/wp-content/plugins/products/updateorder.php", { id: id, value: value},
					function(data) {
						jQuery(jQuery.trim(data)=='1')
						{
							jQuery('.load_'+id).hide();
						}
				});
			}
		});
		
	});
</script>
<form name="conatct_form" method="post" onSubmit="return check_blank();" action="<?php echo $url; ?>">
<input type="hidden" name="page" value="Products" />
<div class="fl">
    <select name="category">
        <option value="0">Select Parent Category</option>
        <?php product_list_all('',$category,'en');?>
    </select>
</div>
<div class="fl">
	<input type="text" name="txt" value="<?php _e($txt); ?>" />
</div>
<div class="fl">
	<input type="submit" name="submitsearch" value="Search" style="cursor:pointer;" />
</div>
<div class="fr">
	<div class="fl" style="margin:5px 5px 0px 0px;">Show Records</div>
	<select name="showtotal" onchange="conatct_form.submit()">
        <option value="10"<?php if($showtotal==10){_e(' selected="selected"');} ?>>10</option>
        <option value="20"<?php if($showtotal==20){_e(' selected="selected"');} ?>>20</option>
        <option value="30"<?php if($showtotal==30){_e(' selected="selected"');} ?>>30</option>
        <option value="40"<?php if($showtotal==40){_e(' selected="selected"');} ?>>40</option>
        <option value="50"<?php if($showtotal==50){_e(' selected="selected"');} ?>>50</option>
        <option value="60"<?php if($showtotal==60){_e(' selected="selected"');} ?>>60</option>
        <option value="70"<?php if($showtotal==70){_e(' selected="selected"');} ?>>70</option>
        <option value="80"<?php if($showtotal==80){_e(' selected="selected"');} ?>>80</option>
        <option value="90"<?php if($showtotal==90){_e(' selected="selected"');} ?>>90</option>
        <option value="100"<?php if($showtotal==100){_e(' selected="selected"');} ?>>100</option>
    </select>
</div>
<div style="clear:both; height:20px;"></div>
	<table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="border:1px solid #ccc;">
		<tr>
			<!--<th valign="top" align="left" width="60">&nbsp;<?php _e("Sr. No." ); ?></th>-->
			<th valign="top" align="left" style="border-left:1px solid #ccc;"><?php _e("Name" ); ?></th>
            <th valign="top" align="left" style="border-left:1px solid #ccc;"><?php _e("Product Number" ); ?></th>
            <th valign="top" align="left" style="border-left:1px solid #ccc;"><?php _e("Category" ); ?></th>
            <th valign="top" align="left" style="border-left:1px solid #ccc;"><?php _e("New" ); ?></th>
            <th valign="top" align="left" style="border-left:1px solid #ccc;"><?php _e("Special" ); ?></th>
            <th valign="top" align="left" style="border-left:1px solid #ccc;"><?php _e("Builder offer" ); ?></th>
            <th valign="top" align="left" style="border-left:1px solid #ccc;"><?php _e("Image" ); ?></th>
            <th valign="top" align="left" style="border-left:1px solid #ccc;"><?php _e("Order By" ); ?></th>
			<th valign="top" align="left" style="border-left:1px solid #ccc;"><?php _e("Actions" ); ?></th>
		</tr>
	<?php $cnt=$limitstart+1; foreach($trips as $trip){ ?>
	  <tr>
		<!--<td valign="top" align="left" style="border-top:1px solid #ccc;">&nbsp;<?php _e($cnt); ?></td>-->
        <td valign="top" align="left" style="border-top:1px solid #ccc; border-left:1px solid #ccc;"><?php _e($trip->product_name); ?></td>
        <td valign="top" align="left" style="border-top:1px solid #ccc; border-left:1px solid #ccc;"><?php _e($trip->product_number); ?></td>
		<td valign="top" align="left" style="border-top:1px solid #ccc; border-left:1px solid #ccc;">
			<?php $cat=product_categories($trip->category_id);_e($cat[0]->name); ?>
        </td>
        <td valign="top" align="left" style="border-top:1px solid #ccc; border-left:1px solid #ccc;">
			<div class="newproduct newproduct_<?php _e($trip->id); ?>">
				<?php if($trip->newproduct=='Y'){?>
                	<a href="javascript:;" coords="<?php _e($trip->id); ?>,newproduct,N"><img src="<?php echo get_option('home'); ?>/wp-content/plugins/products/images/tick.png" /></a>
                <?php }else{ ?>
                	<a href="javascript:;" coords="<?php _e($trip->id); ?>,newproduct,Y"><img src="<?php echo get_option('home'); ?>/wp-content/plugins/products/images/close.png" /></a>
                <?php } ?>
            </div>
        </td>
        <td valign="top" align="left" style="border-top:1px solid #ccc; border-left:1px solid #ccc;">
			<div class="special special_<?php _e($trip->id); ?>">
				<?php if($trip->special=='Y'){?>
                	<a href="javascript:;" coords="<?php _e($trip->id); ?>,special,N"><img src="<?php echo get_option('home'); ?>/wp-content/plugins/products/images/tick.png" /></a>
                <?php }else{ ?>
                	<a href="javascript:;" coords="<?php _e($trip->id); ?>,special,Y"><img src="<?php echo get_option('home'); ?>/wp-content/plugins/products/images/close.png" /></a>
                <?php } ?>
            </div>
		</td>
        <td valign="top" align="left" style="border-top:1px solid #ccc; border-left:1px solid #ccc;">
			<div class="builderoffers builderoffers_<?php _e($trip->id); ?>">
				<?php if($trip->builderoffers=='Y'){?>
                	<a href="javascript:;" coords="<?php _e($trip->id); ?>,builderoffers,N"><img src="<?php echo get_option('home'); ?>/wp-content/plugins/products/images/tick.png" /></a>
                <?php }else{ ?>
                	<a href="javascript:;" coords="<?php _e($trip->id); ?>,builderoffers,Y"><img src="<?php echo get_option('home'); ?>/wp-content/plugins/products/images/close.png" /></a>
                <?php } ?>
            </div>
		</td>
        <td valign="top" align="left" style="border-top:1px solid #ccc; border-left:1px solid #ccc;">
        <?php $pid=$trip->id;
		$productimages=product_images('', " and product_id='$pid' order by orderby"); ?>
        <?php if (file_exists("../wp-content/uploads/products/thumb/".$productimages[0]->image) && trim($productimages[0]->image)!=''){ ?>
              <img style="border:1px solid #ccc;" src="<?php echo get_option('home'); ?>/wp-content/uploads/products/thumb/<?php _e($productimages[0]->image); ?>" width="100" alt="" />
          <?php }else{ ?>
             <img style="border:1px solid #ccc;" src="<?php echo get_option('home'); ?>/wp-content/plugins/products/images/noimage.gif" width="100" alt="" />
             <?php } ?>
        </td>
        <td valign="top" align="left" style="border-top:1px solid #ccc; border-left:1px solid #ccc;">
			<div class="orderby">
            	<div class="load load_<?php _e($trip->id); ?>"><img src="<?php echo get_option('home'); ?>/wp-content/plugins/products/images/indicator.gif" alt="" /></div>
            	<input type="text" name="ord_<?php _e($trip->id); ?>" id="ord_<?php _e($trip->id); ?>" style="width:40px;" value="<?php _e($trip->orderby); ?>" />
            </div>
		</td>
        		<td valign="top" align="left" style="border-top:1px solid #ccc; border-left:1px solid #ccc;">
			<a href="<?php _e($url); ?>&usr=editproduct&id=<?php _e($trip->id); ?>">View and Edit</a>&nbsp;&nbsp;
			<a href="javascript:if(confirm('Please confirm that you would like to delete this?')) {window.location='<?php _e($url); ?>&usr=deleteproduct&id=<?php _e($trip->id); ?>';}">Delete</a>
		</td>
	  </tr>
	  <?php $cnt++; } ?>
	</table>
</form>
<?php if(count($totalphotos)>$totalrec){ $url=get_option('home').'/wp-admin/admin.php?page=Products'.$urlink; ?>
<div style="float:left; margin-top:10px;" class="pagination">

<?php if($pageid>1){ ?><a href="<?php _e($url); ?>" title="First" class="fl"><img src="<?php echo get_option('home');?>/wp-content/plugins/products/images/first.png" alt="First" title="First" /></a><?php } ?>
    <?php $totalpages=ceil(count($totalphotos)/$totalrec);
			
			$previous = $pageid-1;
			if($previous>0)
			{
				?>
				<a class="fl" href="<?php _e($url.'&amp;pagedid='.$previous);?>"><img src="<?php echo get_option('home');?>/wp-content/plugins/products/images/previous.png" alt="previous" title="previous" /></a>
				<?php
			}
			?>
            <div class="fl ml5 mr10">Page Number:</div>
            <div class="fl mr5">
            	<script type="text/javascript">
				//<![CDATA[
					jQuery(document).ready( function(){
						jQuery('#paginate').live('change', function(){
							var pagedid=jQuery(this).val();
							window.location='<?php _e($url); ?>&pagedid='+pagedid;
						});
					})
					//]]>
				</script>
                <select style="float:left;" id="paginate" name="pagedid">
                <?php for($k=1;$k<=$totalpages;$k++){ ?>
                    <option value="<?php _e($k); ?>" <?php if($k==$pageid){ _e('selected="selected"');}?>><?php _e($k); ?></option>
                <?php } ?>
                </select>
           	</div>
			<?php
				
			
			$next = $pageid+1;
			if($totalpages>=$next)
			{
				?>
				<a class="fl" href="<?php _e($url.'&amp;pagedid='.$next);?>"><img src="<?php echo get_option('home');?>/wp-content/plugins/products/images/next.png" alt="next" title="next" /></a>
				<?php
			}
     ?>
     <?php if($totalpages>$pageid){ ?><a href="<?php _e($url.'&amp;pagedid='.$totalpages); ?>" title="Last" class="fl"><img src="<?php echo get_option('home');?>/wp-content/plugins/products/images/last.png" alt="Last" title="Last" /></a><?php } ?>

</div>
<?php } ?>
</div>

<?php } ?>