<?php @session_start();
require_once('../../../wp-config.php');
global $wpdb, $q_config;
$prefix=$wpdb->base_prefix;

$product_id=0;
$category_id=$_POST['category_id'];
if(isset($_POST['product_id']) && trim($_REQUEST['product_id'])!='')
{
	$product_id=$_REQUEST['product_id'];
}
$category_specifications=category_specifications('', " and category_id='$category_id' order by orderby");
if(count($category_specifications)>0)
{
	$showrcords=count($category_specifications);
	
	$i=0;
	foreach($category_specifications as $category_specification)
	{
		$count=0; 
		if(trim($product_id)>0)
		{
			$category_id=$category_specification->category_id;
			$field_key=$category_specification->option_key;
			$specification_id=$category_specification->id;
			$product_specifications=product_specifications('', " and product_id='$product_id' and category_id='$category_id' and specification_id='$specification_id'");
			//echo'<pre>';print_r($product_specifications);echo'</pre>';
			$specifications=$product_specifications[0]->field_value;
		}
		foreach ( qtrans_getSortedLanguages() as $key => $language ) { 
			
		?>
        <?php if($category_specification->field_type=='checkbox' && $count==0){ ?>
        <div class="e-mail">
            <div class="adress"><?php echo getText22($category_specification->option_value,$language); ?> :  </div>
            <div class="field">
                 <input type="checkbox" name="specifications_<?php echo $i; ?>[]"<?php if($specifications=='Yes'){_e(' checked="checked"');} ?> value="Yes" />
            </div>
        </div>
        <?php } else if($category_specification->field_type=='degree' && $count==0){ ?>
        <div class="e-mail">
            <div class="adress"><?php echo getText22($category_specification->option_value,$language); ?> :  </div>
            <div class="field">
                 <input type="text" class="number" name="specifications_<?php echo $i; ?>[]" value="<?php _e($specifications); ?>" />
            </div>
        </div>
        <?php } else if($category_specification->field_type=='number' && $count==0){ ?>
        <div class="e-mail">
            <div class="adress"><?php echo getText22($category_specification->option_value,$language); ?> :  </div>
            <div class="field">
                 <input type="text" class="number" name="specifications_<?php echo $i; ?>[]" value="<?php _e($specifications); ?>" />
            </div>
        </div>
        <?php } else if($category_specification->field_type=='text'){?>
        <div class="e-mail">
            <div class="adress"><?php echo getText22($category_specification->option_value,$language); ?> :  </div>
            <div class="field">
             	<input type="text" name="specifications_<?php echo $i; ?>_<?php echo $count; ?>[]" value="<?php echo getText22($specifications,$language); ?>" />
            </div>
        </div>
        <?php } ?>
        <div class="clr"></div>
        <?php $count++; } ?>
        <div class="clr bord"></div>
        <?php 
		$i++;
	}
}