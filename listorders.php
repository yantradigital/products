<?php if(!isset($_REQUEST['usr']) )
{ 
	global $wpdb,$signature;
	$prefix=$wpdb->base_prefix;
	$blog_id = $wpdb->blogid;
	
	$urlink='';
	$showtotal=10;
	if(isset($_REQUEST['showtotal']) && trim($_REQUEST['showtotal'])!='' && trim($_REQUEST['showtotal'])!='0')
	{
		$showtotal=$_REQUEST['showtotal'];
		$urlink.='&showtotal='.$showtotal;
	}
	
	$totalrec=$showtotal;
	if(isset($_REQUEST['pagedid']) && $_REQUEST['pagedid']>1)
	{
		$pageid=$_REQUEST['pagedid'];
		$limitstart=$totalrec*($pageid-1);
	}
	else
	{
		$pageid=1;
		$limitstart=0;
		$limitsend=$totalrec;
	}
	
	$cond='';
	
	
	
	if(isset($_REQUEST['category']) && trim($_REQUEST['category'])!='' && trim($_REQUEST['category'])!='0')
	{
		$category=$_REQUEST['category'];
		$cond.=" and category_id='$category'";
		$urlink.='&category='.$category;
	}
	
	if(isset($_REQUEST['txt']) && trim($_REQUEST['txt'])!='')
	{
		$txt=$_REQUEST['txt'];
		$txt1=strtolower($txt);
		$cond.=" and ( lower(product_name) like '%$txt1%' || lower(product_number) like '%$txt1%')";
		$urlink.='&txt='.$txt;
	}
	
	$where=" where order_id!='' $cond order by order_id desc";
	$querystr = "SELECT * FROM ".$prefix."orders $where limit $limitstart, $totalrec";
	$trips = $wpdb->get_results($querystr, OBJECT);
	
	$querystr = "SELECT * FROM ".$prefix."orders $where";
	$totalphotos = $wpdb->get_results($querystr, OBJECT);
	$language='en';
?>
<style type="text/css">
table td,table th{padding:5px;}
.pagination{ float:left; line-height:30px; font-size:14px; font-weight:bold;}
.pagination span{background:#f6f6f6; color:#000; padding:0px 10px; text-decoration:underline;}
.pagination a{background:#FFFFFF color:#0000FF; padding:0px 10px; text-decoration:none;}
.pagination a:hover{text-decoration:underline;}
ul.config{	padding:10px;	margin:0px;}
ul.config li{	display:inline;	float:left;	padding:0px 10px;}
ul.config li a{	text-decoration:none;	color:#000066;}
ul.config li a:hover, ul.config li a.active{	text-decoration:underline;	color:#990000;}
.clr{clear:both;}
.fl{float:left;}
.fr{float:right;}
.orderby{position:relative;}
.load{position:absolute; top:6px; left:6px; display:none;}
</style>
<?php $url=get_option('home').'/wp-admin/admin.php?page=Orders'; ?>
<div class="wrap">
<?php    echo "<h2>" . __( 'Manage Orders', 'webserve_trdom' ) . "</h2>"; ?>

<div class="clr"></div>
<?php if(isset($_REQUEST['del'])){if($_REQUEST['del']=='succ'){ ?>
	<div class="updated"><p><strong><?php _e('Deleted successfully.' ); ?></strong></p></div>
<?php }} ?>
<?php if(isset($_REQUEST['add'])){if($_REQUEST['add']=='succ'){ ?>
	<div class="updated"><p><strong><?php _e('Added successfully.' ); ?></strong></p></div>
<?php }} ?>
<?php if(isset($_REQUEST['update'])){if($_REQUEST['update']=='succ'){ ?>
	<div class="updated"><p><strong><?php _e('Update successfully.' ); ?></strong></p></div>
<?php }} ?>
<div class="clr"></div>
<script type="text/javascript" src="<?php echo get_option('home');?>/wp-content/plugins/products/js/jquery.js"></script>
<form name="conatct_form" method="post" onSubmit="return check_blank();" action="<?php echo $url; ?>">
<input type="hidden" name="page" value="Orders" />
<!--<div class="fl">
    <select name="category">
        <option value="0">Select Parent Category</option>
        <?php //product_list_all('',$category,'en'); ?>
    </select>
</div>
<div class="fl">
	<input type="text" name="txt" value="<?php _e($txt); ?>" />
</div>
<div class="fl">
	<input type="submit" name="submitsearch" value="Search" style="cursor:pointer;" />
</div>-->
<div class="fr">
	<div class="fl" style="margin:5px 5px 0px 0px;">Show Records</div>
	<select name="showtotal" onchange="conatct_form.submit()">
        <option value="10"<?php if($showtotal==10){_e(' selected="selected"');} ?>>10</option>
        <option value="20"<?php if($showtotal==20){_e(' selected="selected"');} ?>>20</option>
        <option value="30"<?php if($showtotal==30){_e(' selected="selected"');} ?>>30</option>
        <option value="40"<?php if($showtotal==40){_e(' selected="selected"');} ?>>40</option>
        <option value="50"<?php if($showtotal==50){_e(' selected="selected"');} ?>>50</option>
        <option value="60"<?php if($showtotal==60){_e(' selected="selected"');} ?>>60</option>
        <option value="70"<?php if($showtotal==70){_e(' selected="selected"');} ?>>70</option>
        <option value="80"<?php if($showtotal==80){_e(' selected="selected"');} ?>>80</option>
        <option value="90"<?php if($showtotal==90){_e(' selected="selected"');} ?>>90</option>
        <option value="100"<?php if($showtotal==100){_e(' selected="selected"');} ?>>100</option>
    </select>
</div>
<div style="clear:both; height:20px;"></div>
	<table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="border:1px solid #ccc;">
       <tr>
            <th align="left" valign="top" style="border-left:1px solid #ccc;"><?php _e(getlanguageText('Order ID',$language)); ?></th>
            <th align="left" valign="top" style="border-left:1px solid #ccc;"><?php _e(getlanguageText('Customer',$language)); ?></th>
            <th align="left" valign="top" style="border-left:1px solid #ccc;"><?php _e(getlanguageText('Sales',$language)); ?></th>
            <th align="left" valign="top" style="border-left:1px solid #ccc;"><?php _e(getlanguageText('Date and Time',$language)); ?></th>
            <th align="left" valign="top" style="border-left:1px solid #ccc;"><?php _e(getlanguageText('Status',$language)); ?></th>
            <th align="left" valign="top" style="border-left:1px solid #ccc;"><?php _e(getlanguageText('Action',$language)); ?></th>
            
          </tr>
	<?php $cnt=$limitstart+1; foreach($trips as $order){ ?>
    	<tr>
                <td align="left" valign="top" style="border-top:1px solid #ccc; border-left:1px solid #ccc;"><?php _e($order->order_id); ?></td>
                <td align="left" valign="top" style="border-top:1px solid #ccc; border-left:1px solid #ccc;"><?php echo get_user_meta( $order->user_id, 'first_name', true ); ?> <?php echo get_user_meta( $order->user_id, 'last_name', true ); ?></td>
                <td align="left" valign="top" style="border-top:1px solid #ccc; border-left:1px solid #ccc;"><?php echo get_user_meta( $order->user_parent_id, 'first_name', true ); ?> <?php echo get_user_meta( $order->user_parent_id, 'last_name', true ); ?></td>
                <td align="left" valign="top" style="border-top:1px solid #ccc; border-left:1px solid #ccc;"><?php _e(date('Y-m-d H:i:s',$order->cdate)); ?></td>
                <td align="left" valign="top" style="border-top:1px solid #ccc; border-left:1px solid #ccc;">
                	<?php if(trim($order->order_status)=='P'){_e(getlanguageText('Pending',$language));} ?>
                	<?php if(trim($order->order_status)=='C'){_e(getlanguageText('Confirmed',$language));} ?>
					<?php if(trim($order->order_status)=='D'){_e(getlanguageText('Cancel',$language));} ?>
                </td>
                <td valign="top" align="left" style="border-top:1px solid #ccc; border-left:1px solid #ccc;">
                    <a href="<?php _e($url); ?>&usr=editorder&id=<?php _e($order->order_id); ?>">View and Edit</a>&nbsp;&nbsp;
                    <a href="javascript:if(confirm('Please confirm that you would like to delete this?')) {window.location='<?php _e($url); ?>&usr=deleteorder&id=<?php _e($order->order_id); ?>';}">Delete</a>
                </td>
              </tr>
    
	  <?php $cnt++; } ?>
	</table>
</form>
<?php if(count($totalphotos)>$totalrec){ $url=get_option('home').'/wp-admin/admin.php?page=Orders'.$urlink; ?>
<div style="float:left; margin-top:10px;" class="pagination">

<?php if($pageid>1){ ?><a href="<?php _e($url); ?>" title="First" class="fl"><img src="<?php echo get_option('home');?>/wp-content/plugins/products/images/first.png" alt="First" title="First" /></a><?php } ?>
    <?php $totalpages=ceil(count($totalphotos)/$totalrec);
			
			$previous = $pageid-1;
			if($previous>0)
			{
				?>
				<a class="fl" href="<?php _e($url.'&amp;pagedid='.$previous);?>"><img src="<?php echo get_option('home');?>/wp-content/plugins/products/images/previous.png" alt="previous" title="previous" /></a>
				<?php
			}
			?>
            <div class="fl ml5 mr10">Page Number:</div>
            <div class="fl mr5">
            	<script type="text/javascript">
				//<![CDATA[
					jQuery(document).ready( function(){
						jQuery('#paginate').live('change', function(){
							var pagedid=jQuery(this).val();
							window.location='<?php _e($url); ?>&pagedid='+pagedid;
						});
					})
					//]]>
				</script>
                <select style="float:left;" id="paginate" name="pagedid">
                <?php for($k=1;$k<=$totalpages;$k++){ ?>
                    <option value="<?php _e($k); ?>" <?php if($k==$pageid){ _e('selected="selected"');}?>><?php _e($k); ?></option>
                <?php } ?>
                </select>
           	</div>
			<?php
				
			
			$next = $pageid+1;
			if($totalpages>=$next)
			{
				?>
				<a class="fl" href="<?php _e($url.'&amp;pagedid='.$next);?>"><img src="<?php echo get_option('home');?>/wp-content/plugins/products/images/next.png" alt="next" title="next" /></a>
				<?php
			}
     ?>
     <?php if($totalpages>$pageid){ ?><a href="<?php _e($url.'&amp;pagedid='.$totalpages); ?>" title="Last" class="fl"><img src="<?php echo get_option('home');?>/wp-content/plugins/products/images/last.png" alt="Last" title="Last" /></a><?php } ?>

</div>
<?php } ?>
</div>

<?php } ?>