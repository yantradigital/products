<?php 
global $wpdb, $q_config;
$prefix=$wpdb->base_prefix;
$error=array();
$language=qtrans_getLanguage();

$order_id=$_REQUEST['id'];
$orderitems=order_items(''," and order_id='$order_id'");
$orderdetail=orders($order_id);
$status=$orderdetail[0]->order_status;

$user_id=$orderdetail[0]->user_parent_id;

if(isset($_POST['updatesubmit']) || isset($_POST['updatesubmit2']))
{
	$status = $_POST['status'];
	if(trim($order_id)!='' && trim($status)!='')
	{
		if(trim($status)=='C')
		{
			$orderitems=order_items(''," and order_id='$order_id'");
		
			if(count($orderitems)>0)
			{
				
				$product_list='<table width="100%" cellpadding="2" cellspacing="2" class="orderitems" border="0">';
				foreach($orderitems as $orderitem)
				{
					$product_list.='<tr>
					<td align="left" valign="top">'.$orderitem->product_number.'</td>
					<td align="left" valign="top">'.getText22($orderitem->product_name,$language).'</td>
					<td align="left" valign="top">'.$orderitem->product_quantity.'pcs</td>
					</tr>';
				} 
				$product_list.='</table>';
				
				
				$user_info = get_userdata($orderitems[0]->user_id);
				$salesname=get_user_meta( $user_id, 'first_name', true ).' '.get_user_meta( $user_id, 'last_name', true );
				
				$to  = $user_info->user_email;
				$sitename=get_option('blogname');
				$from = $current_user->user_email;
				
				$headers  = 'MIME-Version: 1.0' . "\r\n";
				$headers .= "Content-type: text/html; charset=utf-8" . "\r\nFrom: $sitename <$from>\r\nReply-To: $from";
				
				$shopsettings=shop_settings(" and setting_key='client_confirmation_subject'");
				$client_confirmation_subject=getText22($shopsettings[0]->setting_value,$language);
				$shopsettings=shop_settings(" and setting_key='client_confirmation_message'");
				
				$msg=getlanguageText('Order Status',$language).' : '.getlanguageText('Confirmed',$language).'<br/><br/>';
				
				$client_confirmation_message=$msg.getText22($shopsettings[0]->setting_value,$language);
				$client_confirmation_message=str_replace('%request_number%',$order_id,$client_confirmation_message);
				$client_confirmation_message=str_replace('%name%',$current_user->user_firstname.' '.$current_user->user_lastname,$client_confirmation_message);
				$client_confirmation_message=str_replace('%product_list%',$product_list,$client_confirmation_message);
				$client_confirmation_message=str_replace('%sales_name%',$salesname,$client_confirmation_message);
				
				wp_mail($to, $client_confirmation_subject, $client_confirmation_message, $headers);
				
				
			}
		}
		$sql="UPDATE `".$prefix."orders` set order_status='$status' where order_id='$order_id'";
		$result = $wpdb->query( $sql );
		
		$sql="UPDATE `".$prefix."order_item` set order_status='$status' where order_id='$order_id'";
		$result = $wpdb->query( $sql );
		
		if(isset($_POST['updatesubmit2']))
		{
			$url=get_option('home').'/wp-admin/admin.php?page=Orders&usr=editorder&update=succ&id='.$order_id;
		}
		else
		{
			$url=get_option('home').'/wp-admin/admin.php?page=Orders&update=succ';
		}
		echo"<script>window.location='".$url."'</script>";
	}
}
?>
<style type="text/css">
.error
{
	color:#CC0000;
}
.donotshowerror label.error
{
	display: none !important;
}
label.error
{
	margin-left:10px;
}
input.error, select.error,textarea.error, checkbox.error
{
	color:#000000;
	border:1px solid #CC0000 !important;
}
input[type='checkbox'].error
{
	border: solid #CC0000;
	outline:1px solid #CC0000 !important;
}
.personal_info{float:left; width:160px;}
table td,table th{padding:5px;}
.e-mail{ clear:both;}
.adress{ width:100px; float:left; text-align:left; font-size:13px; color:#454546;}
.field{ float:left; width:600px;}
.field input, .field select{ width:324px; height:30px; padding:0 !important; border:1px solid #c7cecf;  border:1px solid #c7cecf; margin:0px 0px 10px 0; background:#f0f0f0; }
.field textarea{ width:500px; padding:0 !important; border:1px solid #c7cecf;  border:1px solid #c7cecf; margin:0px 0px 10px 0; background:#f0f0f0; }
.profile .green-submit-btn input[type="submit"], .profile .green-submit-btn input[type="button"]{ width:152px; border:1px solid #b4babb; height: 45px; line-height:45px; text-align:center; color:#000; font-size:17px; font-weight:bold; border-radius:5px; display:block; font-family:Arial, Helvetica, sans-serif; cursor:pointer; }
.profile .green-submit-btn input[type="button"]{ margin-left:20px;}
.field .wp-core-ui input, .field .wp-core-ui select{ width:auto; height:auto;}
input, select, textarea{float:left;}
.clr{clear:both; margin-top:10px;}.mr5{margin-right:5px;}
.fl{float:left;}.removeday, .addday{float:left; color:#FF0000; font-size:18px; text-decoration:none; margin-left:10px;}.addday{color:#0000FF;}
.tt{float:left; width:70px;}
.sparator{width:600px; margin:5px 0px; height:1px; border-bottom:1px solid #000000;} 
.ml10{margin-left:10px;}
.mt10{margin-top:10px;}
.remove{margin-left:10px; margin-top:10px;}
.tabs{float:left; margin-bottom:10px; width:90%; border-bottom:1px solid #000000;}
.tabs a{float:left; padding:5px 5px; margin-right:1px; font-size:14px; color:#666666; background-color:#9999FF; text-decoration:none; outline:none;}
.tabs a.active{ color:#000000; background-color:#fff;}
.destinationprice table{border-bottom: 1px solid #000; padding:5px 0px;}
</style>
<script type="text/javascript" src="<?php echo get_option('home');?>/wp-content/plugins/products/js/jquery.js"></script>
<script type="text/javascript" src="<?php echo get_option('home');?>/wp-content/plugins/products/js/validate.js"></script>

<h2>Order Detail</h2>
<div class="clr"></div>
<?php if(isset($_REQUEST['add'])){if($_REQUEST['add']=='succ'){ ?>
	<div class="updated"><p><strong><?php _e('Added successfully.' ); ?></strong></p></div>
<?php }} ?>
<?php if(isset($_REQUEST['update'])){if($_REQUEST['update']=='succ'){ ?>
	<div class="updated"><p><strong><?php _e('Update successfully.' ); ?></strong></p></div>
<?php }} ?>
<div class="clr"></div>
	<div class="profile donotshowerror">
    	<?php if(count($error)>0)
		  { ?>
		<div class="tabletitle"><span class="error">Error</span></div>
		<table width="700" class="from_main" border="0" cellpadding="0" cellspacing="0">
		  <?php 
		   
			for($i=0;$i<count($error);$i++)
			{
				?>
			  <tr>
				<td align="left" valign="top" class="name"><span class="error"><?php echo $error[$i]; ?></span></td>
			</tr>
	<?php	} ?>
		</table>
		<div class="clr mt20"></div>
	 <?php } ?>
        	
            <div class="orderdetail">
        	<h2><?php _e(getlanguageText('Order ID',$language)); ?>: <?php _e($order_id); ?></h2>
            <?php if(count($orderitems)){ ?>
        <div class="clr"></div>
        	<form id="updatecart" name="updatecart" action="" method="post">
            <input type="hidden" name="id" value="<?php _e($order_id); ?>" />
        	<table width="100%" cellpadding="2" cellspacing="2" class="ordereditems" border="0" style="border:1px solid #ccc;">
            <tr>
                <th align="left" valign="top" style="border-top:1px solid #ccc; border-left:1px solid #ccc;">&nbsp;</th>
                <th align="left" valign="top" style="border-top:1px solid #ccc; border-left:1px solid #ccc;"><?php _e(getlanguageText('Product Number',$language)); ?></th>
                <th align="left" valign="top" style="border-top:1px solid #ccc; border-left:1px solid #ccc;"><?php _e(getlanguageText('Colour',$language)); ?></th>
                <th align="left" valign="top" style="border-top:1px solid #ccc; border-left:1px solid #ccc;"><?php _e(getlanguageText('Factory Number',$language)); ?></th>
                <th align="left" valign="top" style="border-top:1px solid #ccc; border-left:1px solid #ccc;"><?php _e(getlanguageText('Description',$language)); ?></th>
                <th align="left" valign="top" style="border-top:1px solid #ccc; border-left:1px solid #ccc;"><?php _e(getlanguageText('Product',$language)); ?></th>
                <th align="left" valign="top" style="border-top:1px solid #ccc; border-left:1px solid #ccc;"><?php _e(getlanguageText('Quantity',$language)); ?></th>
              </tr>
            <?php foreach($orderitems as $orderitem){ ?>
              <tr>
              	<td align="left" valign="top" style="border-top:1px solid #ccc; border-left:1px solid #ccc;"><?php 
				$pid=$orderitem->product_id;
				$prods=product_details($pid);
				$post_id=$prods[0]->post_id;
				$productimages=product_images('', " and product_id='$pid'");
				 if (file_exists("wp-content/uploads/products/".$productimages[0]->image) && trim($productimages[0]->image)!=''){ ?>
                          <img style="border:1px solid #CCCCCC;" src="<?php echo get_option('home'); ?>/wp-content/plugins/products/imagecrope.php?width=90&amp;maxw=90&amp;height=90&amp;maxh=90&amp;file=<?php echo get_option('home'); ?>/wp-content/uploads/products/<?php _e($productimages[0]->image); ?>" alt="" />
                      <?php }else{ ?>
                        <img style="border:1px solid #CCCCCC;" src="<?php echo get_option('home'); ?>/wp-content/plugins/products/imagecrope.php?width=90&amp;maxw=90&amp;height=90&amp;maxh=90&amp;file=<?php echo get_option('home'); ?>/wp-content/plugins/products/images/noimage.gif" alt="" />
                    <?php } ?>
				 </td>
                <td align="left" valign="top" style="border-top:1px solid #ccc; border-left:1px solid #ccc;"><?php _e($orderitem->product_number); ?></td>
                <td align="left" valign="top" style="border-top:1px solid #ccc; border-left:1px solid #ccc;"><div class="colour" style="width:50px; height:20px; background:<?php _e($orderitem->colour); ?>"></div><span class="colorname"><?php _e($orderitem->pantone_number); ?></span></td>
                <td align="left" valign="top" style="border-top:1px solid #ccc; border-left:1px solid #ccc;"><?php _e($orderitem->factory_number); ?></td>
                <td align="left" valign="top" style="border-top:1px solid #ccc; border-left:1px solid #ccc;"><?php _e($orderitem->description); ?></td>
                <td align="left" valign="top" style="border-top:1px solid #ccc; border-left:1px solid #ccc;"><a href="<?php _e(get_permalink($post_id)); ?>" title="<?php _e($orderitem->product_name); ?>"><?php _e($orderitem->product_name); ?></a></td>
                <td align="left" valign="top" style="border-top:1px solid #ccc; border-left:1px solid #ccc;"><?php _e($orderitem->product_quantity); ?></td>
              </tr>
             <?php } ?>
            </table>
            
            <?php if(trim($orderdetail[0]->msg)!=''){ ?>
            <div class="e-mail">
                    <div class="adress">Message:</div>
                    <div class="field">
                        <?php _e($orderdetail[0]->msg); ?>
                    </div>
                </div>
               <div class="clr"></div>
            <?php } ?>
            <div class="e-mail">
                    <div class="adress">&nbsp;&nbsp;</div>
                    <div class="field" style="margin-top:10px;">
                        <select name="status">
                    	<option value="P"<?php if(trim($status)=='P'){_e(' selected="selected"');} ?>><?php _e(getlanguageText('Pending',$language)); ?></option>
                        <option value="C"<?php if(trim($status)=='C'){_e(' selected="selected"');} ?>><?php _e(getlanguageText('Confirmed',$language)); ?></option>
                        <option value="D"<?php if(trim($status)=='D'){_e(' selected="selected"');} ?>><?php _e(getlanguageText('Cancel',$language)); ?></option>
                    </select>
                    </div>
                </div>
            <div class="e-mail">
                    <div class="adress">&nbsp;&nbsp;</div>
                    <div class="field" style="margin-top:10px;">
                        <div class="green-submit-btn">
                        	<input type="submit" name="updatesubmit" value="Update" class="registration_btn"/> <input style="margin-left:20px;" type="submit" name="updatesubmit2" value="Update and Edit" class="registration_btn"/> <input onclick="return backtolist()" type="button" name="back" value="Back" title="Back" />
                       
                         </div>
                    </div>
                </div>
			</form>
        <?php } ?>
        </div>
        
        </div>
<div class="clr"></div>

<script type="text/javascript">
function backtolist()
{
	window.location='<?php echo get_option('home').'/wp-admin/admin.php?page=Orders'; ?>';
}
</script>
<div class="clr"></div>