<?php 
@session_start();

/*

Plugin Name: Products

Plugin URI: http://www.vantagewebtech.com

Description: This is Products plugin

Author: Rinku Kamboj

Version: 143420

Author URI: http://www.vantagewebtech.com

*/
//*************** Admin function ***************
//this function is used for checking login details


if(!isset($_REQUEST['Manage_Product1']) || ($_REQUEST['usr']=='Manage_Product1') )
{
	function Manage_Product1() {
		include('listproducts.php');
	}
}
if(isset($_REQUEST['usr']) && ($_REQUEST['usr']=='editproduct') )
{
	function Manage_Product2() {
		include('editproduct.php');
	}
	function manageProduct_admin_actions2() {
		add_menu_page("Products", "Products", 1, "Products", "Manage_Product1");
		add_submenu_page("Products", "Products", "Products", 1, "Products", "Manage_Product2");
		add_submenu_page( 'Products', 'Add Product', 'Add Product', '1',  'AddProduct', 'addprod' );
		add_submenu_page( 'Products', 'Categories', 'Categories', '1',  'Categories', 'listcat' );
		add_submenu_page( 'Products', 'Add Category', 'Add Category', '1',  'AddCategory', 'addcat' );
		add_submenu_page( 'Products', 'Orders', 'Orders', '1',  'Orders', 'listorders' );
		add_submenu_page( 'Products', 'Settings', 'Settings', '1',  'Settings', 'settingss' );
	}
	add_action('admin_menu', 'manageProduct_admin_actions2');
}

if(isset($_REQUEST['usr']) && ($_REQUEST['usr']=='deleteproduct') )
{
	function Manage_Product3() {
		include('deleteproduct.php');
	}
	function manageProduct_admin_actions3() {
		add_menu_page("Products", "Products", 1, "Products", "Manage_Product1");
		add_submenu_page("Products", "Products", "Products", 1, "Products", "Manage_Product3");
		add_submenu_page( 'Products', 'Add Product', 'Add Product', '1',  'AddProduct', 'addprod' );
		add_submenu_page( 'Products', 'Categories', 'Categories', '1',  'Categories', 'listcat' );
		add_submenu_page( 'Products', 'Add Category', 'Add Category', '1',  'AddCategory', 'addcat' );
		add_submenu_page( 'Products', 'Orders', 'Orders', '1',  'Orders', 'listorders' );
		add_submenu_page( 'Products', 'Settings', 'Settings', '1',  'Settings', 'settingss' );
	}
	add_action('admin_menu', 'manageProduct_admin_actions3');
}

if(isset($_REQUEST['usr']) && ($_REQUEST['usr']=='editcategory') )
{
	function Manage_Product4() {
		include('editcategory.php');
	}
	function manageProduct_admin_actions4() {
		add_menu_page("Products", "Products", 1, "Products", "Manage_Product1");
		add_submenu_page("Products", "Products", "Products", 1, "Products", "Manage_Product1");
		add_submenu_page( 'Products', 'Add Product', 'Add Product', '1',  'AddProduct', 'addprod' );
		add_submenu_page( 'Products', 'Categories', 'Categories', '1',  'Categories', 'Manage_Product4' );
		add_submenu_page( 'Products', 'Add Category', 'Add Category', '1',  'AddCategory', 'addcat' );
		add_submenu_page( 'Products', 'Orders', 'Orders', '1',  'Orders', 'listorders' );
		add_submenu_page( 'Products', 'Settings', 'Settings', '1',  'Settings', 'settingss' );
	}
	add_action('admin_menu', 'manageProduct_admin_actions4');
}

if(isset($_REQUEST['usr']) && ($_REQUEST['usr']=='deletecategory') )
{
	
	function Manage_Product5() {
		include('deletecategory.php');
	}
	function manageProduct_admin_actions5() {
		add_menu_page("Products", "Products", 1, "Products", "Manage_Product1");
		add_submenu_page("Products", "Products", "Products", 1, "Products", "Manage_Product1");
		add_submenu_page( 'Products', 'Add Product', 'Add Product', '1',  'AddProduct', 'addprod' );
		add_submenu_page( 'Products', 'Categories', 'Categories', '1',  'Categories', 'Manage_Product5' );
		add_submenu_page( 'Products', 'Add Category', 'Add Category', '1',  'AddCategory', 'addcat' );
		add_submenu_page( 'Products', 'Orders', 'Orders', '1',  'Orders', 'listorders' );
		add_submenu_page( 'Products', 'Settings', 'Settings', '1',  'Settings', 'settingss' );
	}
	add_action('admin_menu', 'manageProduct_admin_actions5');
}

if(isset($_REQUEST['usr']) && ($_REQUEST['usr']=='editorder') )
{
	function Manage_Product6() {
		include('editorder.php');
	}
	function manageProduct_admin_actions6() {
		add_menu_page("Products", "Products", 1, "Products", "Manage_Product1");
		add_submenu_page("Products", "Products", "Products", 1, "Products", "Manage_Product1");
		add_submenu_page( 'Products', 'Add Product', 'Add Product', '1',  'AddProduct', 'addprod' );
		add_submenu_page( 'Products', 'Categories', 'Categories', '1',  'Categories', 'listcat' );
		add_submenu_page( 'Products', 'Add Category', 'Add Category', '1',  'AddCategory', 'addcat' );
		add_submenu_page( 'Products', 'Orders', 'Orders', '1',  'Orders', 'Manage_Product6' );
		add_submenu_page( 'Products', 'Settings', 'Settings', '1',  'Settings', 'settingss' );
	}
	add_action('admin_menu', 'manageProduct_admin_actions6');
}

if(isset($_REQUEST['usr']) && ($_REQUEST['usr']=='deleteorder') )
{
	
	function Manage_Product7() {
		include('deleteorder.php');
	}
	function manageProduct_admin_actions7() {
		add_menu_page("Products", "Products", 1, "Products", "Manage_Product1");
		add_submenu_page("Products", "Products", "Products", 1, "Products", "Manage_Product1");
		add_submenu_page( 'Products', 'Add Product', 'Add Product', '1',  'AddProduct', 'addprod' );
		add_submenu_page( 'Products', 'Categories', 'Categories', '1',  'Categories', 'listcat' );
		add_submenu_page( 'Products', 'Add Category', 'Add Category', '1',  'AddCategory', 'addcat' );
		add_submenu_page( 'Products', 'Orders', 'Orders', '1',  'Orders', 'Manage_Product7' );
		add_submenu_page( 'Products', 'Settings', 'Settings', '1',  'Settings', 'settingss' );
	}
	add_action('admin_menu', 'manageProduct_admin_actions7');
}

function manageProduct_admin_actions() {
	
	add_menu_page("Products", "Products", 1, "Products", "Manage_Product1");
	add_submenu_page("Products", "Products", "Products", 1, "Products", "Manage_Product1");
	add_submenu_page( 'Products', 'Add Product', 'Add Product', '1',  'AddProduct', 'addprod' );
	add_submenu_page( 'Products', 'Categories', 'Categories', '1',  'Categories', 'listcat' );
	add_submenu_page( 'Products', 'Add Category', 'Add Category', '1',  'AddCategory', 'addcat' );
	add_submenu_page( 'Products', 'Orders', 'Orders', '1',  'Orders', 'listorders' );
	add_submenu_page( 'Products', 'Settings', 'Settings', '1',  'Settings', 'settingss' );
}

if(!isset($_REQUEST['usr']) )
{
	add_action('admin_menu', 'manageProduct_admin_actions');
}

function addprod()
{
	include('addproduct.php');
}
function addcat()
{
	include('addcategory.php');
}
function listcat()
{
	include('listcategories.php');
}
function settingss()
{
	include('settings.php');
}
function listorders()
{
	include('listorders.php');
}
function createalias($alias,$cnt='',$id='')
{
	global $wpdb;
	$prefix=$wpdb->base_prefix;
	
	if(trim($cnt)!='')
	{
		$alias=$alias.'-'.$cnt;
	}
	else
	{
		$cnt=1;
	}
	$cond='';
	if(trim($id)!='')
	{
		$cond.=" and id!='$id'";
	}
		
	$check=product_details('', " and alias='$alias' $cond");
	$cnt++;
	if(count($check)>0)
	{
		return createalias($alias,$cnt,$id);
	}
	else
	{
		return $alias;
	}
}
function getText22($text,$lang='')
{
	$tt=explode('<!--:'.$lang.'-->',$text);
	$ttt=explode('<!--:-->',$tt[1]);
	
	if(count($tt)>1)
	{
		return str_replace("&","&amp;",$ttt[0]);
	}
	else
	{
		return $text;
	}
}
function list_tree($cid="", $childis='0', $level='0',$chk,$selected,$self,$lang) 
{
	global $wpdb;
	$prefix=$wpdb->base_prefix;

	$level++;
	//if(count($chk)<=0)
	//{
		
		$querystr = "SELECT * FROM ".$prefix."product_category where parent='$cid' and parent!='0'";
		$parentgs = $wpdb->get_results($querystr, OBJECT);
		if(count($parentgs)>0)
		{
			foreach($parentgs as $parentg)
			{
				if ($parentg->id != $childis) 
				{
					if($selected==$parentg->id)
					{
						$selectted='selected';
					}
					else
					{
						$selectted='';
					}
					if($self!=$parentg->id)
					{
						echo'<option value="'.$parentg->id.'" '.$selectted.'>';
						for ($i=0;$i<$level;$i++) 
						{
							echo "&#151;";
						}
						//echo "|$level|";
						echo "&nbsp;" . getText22($parentg->name,$lang)."</option>";
					}
				}
				
				list_tree($parentg->id,$parentg->parentcat,$level,$chk,$selected,$self,$lang);
			}
		}
	//}
}

function list_all($self='',$selected='',$lang='')
{
	
	global $wpdb;
	$prefix=$wpdb->base_prefix;
	
	
	$querystr = "SELECT * FROM ".$prefix."product_category order by id";
	$parentgs = $wpdb->get_results($querystr, OBJECT);
	
	$chk=array();
	foreach($parentgs as $parentg)
	{
		$pareid=0;
		if($parentg->parent==0)
		{
			if($selected==$parentg->id)
			{
				$selectted='selected';
			}
			else
			{
				$selectted='';
			}
			$pareid=$parentg->id;
			if($self!=$parentg->id)
			echo'<option value="'.$parentg->id.'" '.$selectted.'>'.getText22($parentg->name,$lang)."</option>";
			
		}
		else
		{
			array_push($chk,$parentg->id);
		}
		list_tree($pareid,'0', '0',$chk,$selected,$self,$lang);
	}
}
function product_list_tree($cid="", $childis='0', $level='0',$chk,$selected,$self,$lang) 
{
	global $wpdb;
	$prefix=$wpdb->base_prefix;

	$level++;
	//if(count($chk)<=0)
	//{
		
		$querystr = "SELECT * FROM ".$prefix."product_category where parent='$cid' and parent!='0' ";
		$parentgs = $wpdb->get_results($querystr, OBJECT);
		if(count($parentgs)>0)
		{
			foreach($parentgs as $parentg)
			{
				if ($parentg->id != $childis) 
				{
					if($selected==$parentg->id)
					{
						$selectted='selected';
					}
					else
					{
						$selectted='';
					}
					if($self!=$parentg->id)
					{
						echo'<option value="'.$parentg->id.'" '.$selectted.'>';
						for ($i=0;$i<$level;$i++) 
						{
							echo "&#151;";
						}
						//echo "|$level|";
						echo "&nbsp;" . getText22($parentg->name,$lang)."</option>";
					}
				}
				
				product_list_tree($parentg->id,$parentg->parentcat,$level,$chk,$selected,$self,$lang);
			}
		}
	//}
}

function product_list_all($self='',$selected='',$lang='')
{
	
	global $wpdb;
	$prefix=$wpdb->base_prefix;
	
	
	$querystr = "SELECT * FROM ".$prefix."product_category order by id";
	$parentgs = $wpdb->get_results($querystr, OBJECT);
	
	$chk=array();
	foreach($parentgs as $parentg)
	{
		$pareid=0;
		if($parentg->parent==0)
		{
			if($selected==$parentg->id)
			{
				$selectted='selected';
			}
			else
			{
				$selectted='';
			}
			$pareid=$parentg->id;
			if($self!=$parentg->id)
			echo'<optgroup label="'.getText22($parentg->name,$lang).'">';
			
		}
		else
		{
			array_push($chk,$parentg->id);
		}
		product_list_tree($pareid,'0', '0',$chk,$selected,$self,$lang);
		echo'</optgroup>';
	}
	//print_r($chk);
}
function product_categories($id='', $cnd='')
{
	global $wpdb;
	$prefix=$wpdb->base_prefix;
	$cond='';
	if(trim($id)!='')
	{
		$cond.=" and id='$id'";
	}
	if($cnd!='')
	{
		$cond.=" $cnd";
	}
	$querystr = "SELECT * FROM ".$prefix."product_category where id!='' $cond";
	$data = $wpdb->get_results($querystr, OBJECT);
	return $data;
}
function category_terms($cnd='')
{
	global $wpdb;
	$prefix=$wpdb->base_prefix;
	$cond='';
	if($cnd!='')
	{
		$cond.=" $cnd";
	}
	$querystr = "SELECT * FROM ".$prefix."term_taxonomy where term_taxonomy_id!='' $cond";
	$data = $wpdb->get_results($querystr, OBJECT);
	return $data;
}
function category_specifications($id='', $cnd='')
{
	global $wpdb;
	$prefix=$wpdb->base_prefix;
	$cond='';
	if(trim($id)!='')
	{
		$cond.=" and id='$id'";
	}
	if($cnd!='')
	{
		$cond.=" $cnd";
	}
	$querystr = "SELECT * FROM ".$prefix."category_specifications where id!='' $cond";
	$data = $wpdb->get_results($querystr, OBJECT);
	return $data;
}
function category_term($cnd='')
{
	global $wpdb;
	$prefix=$wpdb->base_prefix;
	$cond='';
	if($cnd!='')
	{
		$cond.=" $cnd";
	}
	$querystr = "SELECT * FROM ".$prefix."terms where term_id!='' $cond";
	$data = $wpdb->get_results($querystr, OBJECT);
	return $data;
}
function product_search($cnd='')
{
	global $wpdb;
	$prefix=$wpdb->base_prefix;
	$cond='';
	if($cnd!='')
	{
		$cond.=" $cnd";
	}
	$querystr = "SELECT p.* FROM ".$prefix."products as p, ".$prefix."product_specifications as ps where p.id=ps.product_id  $cond";
	$data = $wpdb->get_results($querystr, OBJECT);
	return $data;
}
function product_details($id='', $cnd='')
{
	global $wpdb;
	$prefix=$wpdb->base_prefix;
	$cond='';
	if(trim($id)!='' && trim($id)!='0')
	{
		$cond.=" and id='$id'";
	}
	if($cnd!='')
	{
		$cond.=" $cnd";
	}
	$querystr = "SELECT * FROM ".$prefix."products where id!='' $cond";
	$data = $wpdb->get_results($querystr, OBJECT);
	return $data;
}
function product_specifications($id='', $cnd='')
{
	global $wpdb;
	$prefix=$wpdb->base_prefix;
	$cond='';
	if(trim($id)!='' && trim($id)!='0')
	{
		$cond.=" and id='$id'";
	}
	if($cnd!='')
	{
		$cond.=" $cnd";
	}
	$querystr = "SELECT * FROM ".$prefix."product_specifications where id!='' $cond";
	$data = $wpdb->get_results($querystr, OBJECT);
	return $data;
}
function product_images($id='', $cnd='')
{
	global $wpdb;
	$prefix=$wpdb->base_prefix;
	$cond='';
	if(trim($id)!='' && trim($id)!='0')
	{
		$cond.=" and id='$id'";
	}
	if($cnd!='')
	{
		$cond.=" $cnd";
	}
	$querystr = "SELECT * FROM ".$prefix."product_images where id!='' $cond";
	$data = $wpdb->get_results($querystr, OBJECT);
	return $data;
}

function product_attributes($id='', $cnd='',$field='')
{
	global $wpdb;
	$prefix=$wpdb->base_prefix;
	$cond='';
	if(trim($id)!='' && trim($id)!='0')
	{
		$cond.=" and id='$id'";
	}
	if($cnd!='')
	{
		$cond.=" $cnd";
	}
	
	$fields='*';
	if(trim($field)!='')
	{
		$fields=$field;
	}
	
	$querystr = "SELECT $fields FROM ".$prefix."product_attributes where id!='' $cond";
	$data = $wpdb->get_results($querystr, OBJECT);
	return $data;
}

function AllCategories($attr)
{
	global $wpdb, $q_config, $post;
	$prefix=$wpdb->base_prefix;
	$cond='';
	$id=$attr['id'];
	$parent=$attr['parent'];
	if(trim($id)!='')
	{
		$cond.=" and id='$id'";
	}
	if(trim($parent)!='')
	{
		$cond.=" and parent='$parent'";
	}
	$querystr = "SELECT * FROM ".$prefix."product_category where id!='' $cond";
	$categories = $wpdb->get_results($querystr, OBJECT);
	$data='';
	$language=qtrans_getLanguage();
	if(count($categories)>0)
	{
		$i=1;
		$data.='<div class="category-page">';
		foreach($categories as $category)
		{
			$tid=$category->term_id;
			$terms=category_term(" and term_id='$tid'");
			$termlink='';
			if(count($terms)>0)
			{
				$termlink=get_option('home').'/?product_category='.$terms[0]->slug;
			}
			if($i%2==0)
			{
				
				$data.='<div class="category-detail last"><div class="category-header"></div>';
				$data.='<h1>'.getText22($category->name,$language).'</h1>';
				$data.='<div class="cat-content">'.getText22($category->description,$language).'</div>';
				if(trim($termlink)!='')
				{
					$data.='<div class="range-link"><a href="'.$termlink.'" title="View Products Range">View Products Range</a></div>';
				}
				$data.='<div class="category-footer"></div></div>';
				$data.='<div class="clr"></div>';
			}
			else
			{
				$data.='<div class="category-detail"><div class="category-header"></div>';
				$data.='<h1>'.getText22($category->name,$language).'</h1>';
				$data.='<div class="cat-content">'.getText22($category->description,$language).'</div>';
				if(trim($termlink)!='')
				{
					$data.='<div class="range-link"><a href="'.$termlink.'" title="View Products Range">View Products Range</a></div>';
				}
				$data.='<div class="category-footer"></div></div>';
			}
			$i++;
		}
		$data.='</div>';
	}
	return $data;
}
add_shortcode('Product Categories', 'AllCategories');

function homecategories($attr)
{
	global $wpdb, $q_config, $post;
	$prefix=$wpdb->base_prefix;
	
	$querystr = "SELECT * FROM ".$prefix."product_category where parent='0'";
	$categories = $wpdb->get_results($querystr, OBJECT);
	$data='';
	$language=qtrans_getLanguage();
	$permalink_structure = get_option('permalink_structure');
	
	if(count($categories)>0)
	{
		$i=1;
		$data.='<div id="currentproimages">
	<div id="carousel" class="outerWrapper">';
		foreach($categories as $category)
		{
			if(trim(getText22($category->name,$language))!='')
			{
				$tid=$category->term_id;
				$terms=category_term(" and term_id='$tid'");
				$termlink='';
				if(count($terms)>0)
				{
					if(trim($permalink_structure)!='')
					{
						$termlink=get_option('home').'/product_category/'.$terms[0]->slug.'/?lang='.$language;
					}
					else
					{
						$termlink=get_option('home').'/?product_category='.$terms[0]->slug.'&amp;lang='.$language;
					}
					if($terms[0]->slug=='bathroom')
					{
						$termlink='javascript:;';
					}
				}
				$img='';
				
				if (file_exists("wp-content/uploads/products/cat/".$category->image) && trim($category->image)!=''){ 
				  $img='<img src="'.get_option('home').'/wp-content/plugins/products/imagecrope.php?width=256&amp;maxw=256&amp;height=103&amp;maxh=103&amp;file='.get_option('home').'/wp-content/uploads/products/cat/'.$category->image.'" alt="" />';
				 }else{ 
					$img='<img src="'.get_option('home').'/wp-content/plugins/products/imagecrope.php?width=256&amp;maxw=256&amp;height=103&amp;maxh=103&amp;file='.get_option('home').'/wp-content/plugins/products/images/noimage.gif" alt="" />';
				 }
				$submenu='';
				$mainclass='';
				if($terms[0]->slug=='bathroom')
				{
					$parent=$category->id;
					$subcats=product_categories('', " and parent='$parent' order by orderby");
					if(count($subcats)>0)
					{
						?>
                        <script type="text/javascript">
						   jQuery(document).ready( function(){
							   jQuery('.bath, .bathroomsubmenu').live('mouseenter', function(){
									jQuery('.bathroomsubmenu').show();
							   });
							   jQuery('.bath, .bathroomsubmenu').live('mouseleave', function(){
									jQuery('.bathroomsubmenu').hide();
							   });
						   });
						   </script>
                        <?php
						$mainclass=' class="bath"';
						$submenu.='<div class="bathroomsubmenu" style="display:none;"><ul>';
						foreach($subcats as $subcat)
						{
							$tid2=$subcat->term_id;
							$terms2=category_term(" and term_id='$tid2'");
							$termlink2='';
							if(count($terms2)>0)
							{
								if(trim($permalink_structure)!='')
								{
									$termlink2=get_option('home').'/product_category/'.$terms2[0]->slug.'/?lang='.$language;
								}
								else
								{
									$termlink2=get_option('home').'/?product_category='.$terms2[0]->slug.'&amp;lang='.$language;
								}
							}
							$submenu.='<li><a href="'.$termlink2.'" title="'.getText22($subcat->name,$language).'">'.getText22($subcat->name,$language).'</a></li>';
						}
						$submenu.='</ul></div>';
					}
				}
				$data.='<div class="item"><div class="img1">';
				$data.='<a'.$mainclass.' href="'.$termlink.'" title="'.getText22($category->name,$language).'">'.getText22($category->name,$language);
				$data.=$img.'</a></div>'.$submenu.'</div>';
				
				$i++;
			}
		}
		$data.='</div><div class="imgreep"><span></span></div></div>';
	}
	return $data;
}
add_shortcode('Home Top Categories', 'homecategories');

function orders($id='',$cnd='')
{
	global $wpdb, $q_config, $post;
	$prefix=$wpdb->base_prefix;
	$cond='';
	if(trim($id)!='')
	{
		$cond.=" and order_id='$id'";
	}
	if($cnd!='')
	{
		$cond.=" $cnd";
	}
	$querystr = "SELECT * FROM ".$prefix."orders where order_id!='' $cond";
	$data = $wpdb->get_results($querystr, OBJECT);
	return $data;
}

function order_items($id='',$cnd='')
{
	global $wpdb, $q_config, $post;
	$prefix=$wpdb->base_prefix;
	$cond='';
	if(trim($id)!='')
	{
		$cond.=" and order_item_id='$id'";
	}
	if($cnd!='')
	{
		$cond.=" $cnd";
	}
	$querystr = "SELECT * FROM ".$prefix."order_item where order_item_id!='' $cond";
	$data = $wpdb->get_results($querystr, OBJECT);
	return $data;
}

function add_toshop($attrid,$qunt)
{
	global $wpdb, $q_config, $post;
	$prefix=$wpdb->base_prefix;
	$current_user = wp_get_current_user();
	$user_id=$current_user->ID;
	$time=time();
	$allusers=allusers(" and ID='$user_id'");
	
	$user_parent_id=$allusers[0]->parent_id;
	
	$cond='add';
	$order_item_id=0;
	if(!isset($_SESSION['order_id']) || (trim($_SESSION['order_id'])==''))
	{
		$lastorder=orders(''," and user_id='$user_id' and order_status='R'");
		if(count($lastorder)>0)
		{
			$order_id=$lastorder[0]->order_id;
			$_SESSION['order_id']=$order_id;
			
			$order_items=order_items(''," and user_id='$user_id' and order_status='R' and order_id='$order_id' and attr_id='$attrid'");
			if(count($order_items)>0)
			{
				$cond='update';
				$qunt=$qunt+$order_items[0]->product_quantity;
				$order_item_id=$order_items[0]->order_item_id;
			}
		}
		else
		{
			$ip_address=$_SERVER['REMOTE_ADDR'];
			$sql="INSERT INTO `".$prefix."orders` (`user_id`, `user_parent_id`, `order_status`, `cdate`, `ip_address`) VALUES ('$user_id', '$user_parent_id', 'R', '$time', '$ip_address')";
			$result = $wpdb->query( $sql );
			$order_id=$wpdb->insert_id;
			$_SESSION['order_id']=$order_id;
		}
		
	}
	else
	{
		$order_id=$_SESSION['order_id'];
		$order_items=order_items(''," and user_id='$user_id' and order_status='R' and order_id='$order_id' and attr_id='$attrid'");
		if(count($order_items)>0)
		{
			$cond='update';
			$qunt=$qunt+$order_items[0]->product_quantity;
			$order_item_id=$order_items[0]->order_item_id;
		}
	}
	
	$productattributes=product_attributes($attrid);
	$productattribute=$productattributes[0];
	
	$product_id=$productattribute->product_id;
	$product_number=$productattribute->product_number;
	$colour=$productattribute->colour;
	$factory_number=$productattribute->factory_number;
	$stock_states1=$productattribute->stock_states1;
	$stock_states2=$productattribute->stock_states2;
	$pantone_number=$productattribute->pantone_number;
	$description=$productattribute->description;
	
	$productdetails=product_details($product_id);
	
	$product_name=$productdetails[0]->product_name;
	
	if($cond=='add')
	{
		$sql="INSERT INTO `".$prefix."order_item` (`order_id`, `user_id`, `user_parent_id`, `product_id`, `product_name`, `product_number`, `colour`, `factory_number`, `stock_states1`, `stock_states2`, `product_quantity`, `order_status`, `cdate`, `pantone_number`, `attr_id`, `description`) VALUES ('$order_id', '$user_id', '$user_parent_id', '$product_id', '$product_name', '$product_number', '$colour', '$factory_number', '$stock_states1', '$stock_states2', '$qunt', 'R', '$time', '$pantone_number', '$attrid', '$description')";
		$result = $wpdb->query( $sql );
		if($result==1)
		{
			return true;	
		}
		else
		{
			return false;	
		}
	}
	else if($cond=='update')
	{
		$sql="UPDATE `".$prefix."order_item` set product_quantity='$qunt', mdate='$time' where user_id='$user_id' and order_status='R' and order_id='$order_id' and attr_id='$attrid' and order_item_id='$order_item_id'";
		$result = $wpdb->query( $sql );
		if($result==1)
		{
			return true;	
		}
		else
		{
			return false;	
		}
	}
}

function update_shop($attrid,$qunt, $order_id, $order_item_id)
{
	global $wpdb, $q_config, $post;
	$prefix=$wpdb->base_prefix;
	$current_user = wp_get_current_user();
	$user_id=$current_user->ID;
	$time=time();
	
	$sql="UPDATE `".$prefix."order_item` set product_quantity='$qunt', mdate='$time' where user_id='$user_id' and order_status='R' and order_id='$order_id' and attr_id='$attrid' and order_item_id='$order_item_id'";
	$result = $wpdb->query( $sql );
	if($result==1)
	{
		return true;	
	}
	else
	{
		return false;	
	}
}
function shop_settings($cnd='')
{
	global $wpdb;
	$prefix=$wpdb->base_prefix;
	$cond='';
	if($cnd!='')
	{
		$cond.=" $cnd";
	}
	$querystr = "SELECT * FROM ".$prefix."shop_settings where id!='' $cond";
	$data = $wpdb->get_results($querystr, OBJECT);
	return $data;
}

function product_breadcrumb() {

   global $post, $wpdb, $q_config;
  $homeLink = get_bloginfo('url');
  $language=qtrans_getLanguage();
  $permalink_structure = get_option('permalink_structure');
  
  $showOnHome = 0; // 1 - show breadcrumbs on the homepage, 0 - don't show
  $delimiter = '<span class="saparator">></span>'; // delimiter between crumbs
  $home = 'Home'; // text for the 'Home' link
  $ourproduct = getlanguageText('Our Products',$language); 
  $productcomparison = getlanguageText('Product Comparison',$language);
  $showCurrent = 1; // 1 - show current post/page title in breadcrumbs, 0 - don't show
  $before = '<span class="current">'; // tag before the current crumb
  $after = '</span>'; // tag after the current crumb
  $taxonomy='product_category';
  $link4='';
  $link3='';
  $link2='';
  $link1='';
  $typetitle='';
  $iscate=false;
  
	$typeurl='';
	$type='';
	if(isset($_REQUEST['type']) && trim($_REQUEST['type'])!='')
	{
		$type=trim($_REQUEST['type']);
		$typeurl='&amp;type='.$type;
		
		if($type=='builderoffers')
		{
			$typetitle=getlanguageText('Builders offers',$language);
		}
		if($type=='special')
		{
			$typetitle=getlanguageText('Special offers',$language);
		}
	}
  	
    ?>
    <div id="crumbs">
    	<a href="<?php echo $homeLink.'?lang='.$language;?>"><?php _e($home); ?></a> <?php _e($delimiter); ?>
  		<span><?php _e($ourproduct); ?></span> <?php _e($delimiter); ?>
        
        <?php if(isset($_REQUEST['view']) && trim($_REQUEST['view'])=='campare'){ ?>
			
			<span><?php _e($productcomparison); ?></span> 
		<?php
		 }
		 else{ 
		 if(!is_single() && !is_page()){
		 $term_id=get_queried_object()->term_id;
		   $curentcat=product_categories(''," and term_id='$term_id'");
		   $catid=0;
		   $catid=$curentcat[0]->id;
		   $parentid=$curentcat[0]->parent;
		   if(trim($parentid)!=0)
		   {
				$parentcat=product_categories($parentid);
				if(count($parentcat)>0)
				{
					$tid=$parentcat[0]->term_id;
					$terms=category_term(" and term_id='$tid'");
					if(count($terms)>0)
					{
						if(trim($permalink_structure)!='')
						{
							$termlink=get_option('home').'/product_category/'.$terms[0]->slug.'/?lang='.$language.$typeurl;
						}
						else
						{
							$termlink=get_option('home').'/?product_category='.$terms[0]->slug.'&amp;lang='.$language.$typeurl;
						}
						if($terms[0]->slug=='bathroom')
						{
							$link1=' <span>'.getText22($parentcat[0]->name,$language).'</span> '.$delimiter;
						}
						else
						{
							$link1=' <a href="'.$termlink.'">'.getText22($parentcat[0]->name,$language).'</a> '.$delimiter;
						}
                        
					}
				}
				
				$parentid=$parentcat[0]->parent;
			   	if(trim($parentid)!=0)
			   	{
					$parentcat=product_categories($parentid);
					if(count($parentcat)>0)
					{
						$tid=$parentcat[0]->term_id;
						$terms=category_term(" and term_id='$tid'");
						if(count($terms)>0)
						{
							if(trim($permalink_structure)!='')
							{
								$termlink=get_option('home').'/product_category/'.$terms[0]->slug.'/?lang='.$language.$typeurl;
							}
							else
							{
								$termlink=get_option('home').'/?product_category='.$terms[0]->slug.'&amp;lang='.$language.$typeurl;
							}
							if($terms[0]->slug=='bathroom')
							{
								$link2=' <span>'.getText22($parentcat[0]->name,$language).'</span> '.$delimiter;
							}
							else
							{
								$link2=' <a href="'.$termlink.'">'.getText22($parentcat[0]->name,$language).'</a> '.$delimiter;
							}
							
						}
					}
					$parentid=$parentcat[0]->parent;
				   	if(trim($parentid)!=0)
				   	{
						$parentcat=product_categories($parentid);
						if(count($parentcat)>0)
						{
							$tid=$parentcat[0]->term_id;
							$terms=category_term(" and term_id='$tid'");
							if(count($terms)>0)
							{
								if(trim($permalink_structure)!='')
								{
									$termlink=get_option('home').'/product_category/'.$terms[0]->slug.'/?lang='.$language.$typeurl;
								}
								else
								{
									$termlink=get_option('home').'/?product_category='.$terms[0]->slug.'&amp;lang='.$language.$typeurl;
								}
								if($terms[0]->slug=='bathroom')
								{
									$link3=' <span>'.getText22($parentcat[0]->name,$language).'</span> '.$delimiter;
								}
								else
								{
									$link3=' <a href="'.$termlink.'">'.getText22($parentcat[0]->name,$language).'</a> '.$delimiter;
								}
							}
						}
						$parentid=$parentcat[0]->parent;
					   	if(trim($parentid)!=0)
					   	{
							$parentcat=product_categories($parentid);
							if(count($parentcat)>0)
							{
								$tid=$parentcat[0]->term_id;
								$terms=category_term(" and term_id='$tid'");
								if(count($terms)>0)
								{
									if(trim($permalink_structure)!='')
									{
										$termlink=get_option('home').'/product_category/'.$terms[0]->slug.'/?lang='.$language.$typeurl;
									}
									else
									{
										$termlink=get_option('home').'/?product_category='.$terms[0]->slug.'&amp;lang='.$language.$typeurl;
									}
									$link4=' <a href="'.$termlink.'">'.getText22($parentcat[0]->name,$language).'</a> '.$delimiter;
								}
							}
						}
					}
				}
			}
			echo $link4.$link3.$link2.$link1;
			if(count($curentcat)>0)
			{
				$iscate=true;
				?>
				<span><?php _e($curentcat[0]->name); ?></span>
			  <?php 
			 }  
          }}
		  if(is_single() && !is_page())
		  {
		  	$post_id=$post->ID;
			$products=product_details('', " and post_id='$post_id'");
			$parentid=$products[0]->category_id;
		   	if(trim($parentid)!=0)
		   	{
				$parentcat=product_categories($parentid);
				if(count($parentcat)>0)
				{
					$tid=$parentcat[0]->term_id;
					$terms=category_term(" and term_id='$tid'");
					if(count($terms)>0)
					{
						if(trim($permalink_structure)!='')
						{
							$termlink=get_option('home').'/product_category/'.$terms[0]->slug.'/?lang='.$language.$typeurl;
						}
						else
						{
							$termlink=get_option('home').'/?product_category='.$terms[0]->slug.'&amp;lang='.$language.$typeurl;
						}
						if($terms[0]->slug=='bathroom')
						{
							$link1=' <span>'.getText22($parentcat[0]->name,$language).'</span> '.$delimiter;
						}
						else
						{
							$link1=' <a href="'.$termlink.'">'.getText22($parentcat[0]->name,$language).'</a> '.$delimiter;
						}
                        
					}
				}
				
				$parentid=$parentcat[0]->parent;
			   	if(trim($parentid)!=0)
			   	{
					$parentcat=product_categories($parentid);
					if(count($parentcat)>0)
					{
						$tid=$parentcat[0]->term_id;
						$terms=category_term(" and term_id='$tid'");
						if(count($terms)>0)
						{
							if(trim($permalink_structure)!='')
							{
								$termlink=get_option('home').'/product_category/'.$terms[0]->slug.'/?lang='.$language.$typeurl;
							}
							else
							{
								$termlink=get_option('home').'/?product_category='.$terms[0]->slug.'&amp;lang='.$language.$typeurl;
							}
							if($terms[0]->slug=='bathroom')
							{
								$link2=' <span>'.getText22($parentcat[0]->name,$language).'</span> '.$delimiter;
							}
							else
							{
								$link2=' <a href="'.$termlink.'">'.getText22($parentcat[0]->name,$language).'</a> '.$delimiter;
							}
							//$link2=' <a href="'.$termlink.'">'.getText22($parentcat[0]->name,$language).'</a> '.$delimiter;
						}
					}
					$parentid=$parentcat[0]->parent;
				   	if(trim($parentid)!=0)
				   	{
						$parentcat=product_categories($parentid);
						if(count($parentcat)>0)
						{
							$tid=$parentcat[0]->term_id;
							$terms=category_term(" and term_id='$tid'");
							if(count($terms)>0)
							{
								if(trim($permalink_structure)!='')
								{
									$termlink=get_option('home').'/product_category/'.$terms[0]->slug.'/?lang='.$language.$typeurl;
								}
								else
								{
									$termlink=get_option('home').'/?product_category='.$terms[0]->slug.'&amp;lang='.$language.$typeurl;
								}
								if($terms[0]->slug=='bathroom')
								{
									$link3=' <span>'.getText22($parentcat[0]->name,$language).'</span> '.$delimiter;
								}
								else
								{
									$link3=' <a href="'.$termlink.'">'.getText22($parentcat[0]->name,$language).'</a> '.$delimiter;
								}
								//$link3=' <a href="'.$termlink.'">'.getText22($parentcat[0]->name,$language).'</a> '.$delimiter;
							}
						}
						$parentid=$parentcat[0]->parent;
					   	if(trim($parentid)!=0)
					   	{
							$parentcat=product_categories($parentid);
							if(count($parentcat)>0)
							{
								$tid=$parentcat[0]->term_id;
								$terms=category_term(" and term_id='$tid'");
								if(count($terms)>0)
								{
									if(trim($permalink_structure)!='')
									{
										$termlink=get_option('home').'/product_category/'.$terms[0]->slug.'/?lang='.$language.$typeurl;
									}
									else
									{
										$termlink=get_option('home').'/?product_category='.$terms[0]->slug.'&amp;lang='.$language.$typeurl;
									}
									$link4=' <a href="'.$termlink.'">'.getText22($parentcat[0]->name,$language).'</a> '.$delimiter;
								}
							}
						}
					}
				}
			}
			echo $link4.$link3.$link2.$link1;
			?>
            <span><?php _e($products[0]->product_name); ?></span>
            <?php
		  } 
		  else
		  {
		  	if(!is_page() && $iscate==false)
			{
				?>
				<span><?php _e($typetitle); ?></span>
				<?php
			}
			
		  }
		  if(is_page())
		  {
		  	$post_title=$post->post_title;
			?>
            <span><?php _e($post_title); ?></span>
            <?php
		  }
		  if(is_search())
		  {
			?>
            <span><?php _e('Search'); ?></span>
            <?php
		  }
		  ?>
    </div>
    <?php
  
  
} // end qt_custom_breadcrumbs()

function othermenus($type='')
{
	global $post, $wpdb, $q_config;
	$prefix=$wpdb->base_prefix;
	$homeLink = get_bloginfo('url');
	$language=qtrans_getLanguage();
	$permalink_structure = get_option('permalink_structure');
	
	$cond='';
	if(trim($type)!='')
	{
		$cond.=" and $type='Y'";
	}
	
	$querystr = "SELECT category_id FROM ".$prefix."products where id!='' $cond group by category_id order by category_id";
	$productcategories = $wpdb->get_results($querystr, OBJECT);
	$menu='';
	if(count($productcategories)>0)
	{
		$cats='';
		$i=1;
		foreach($productcategories as $productcategory)
		{
			if($i==1)
			{
				$cats.=$productcategory->category_id;
			}
			else
			{
				$cats.=','.$productcategory->category_id;
			}
			$i++;
		}
		$categories=product_categories(''," and id in ($cats) group by parent order by parent");
		if(count($categories)>0)
		{
			$menu.='<ul class="sub-menu">';
			$menu2='';
			$menuarray=array();
			//$j=1;
			foreach($categories as $category)
			{
				$menu3='';
				$menu4='';
				$menu5='';
				$parentid33=$category->id;
				
				$categories33=product_categories(''," and parent=$parentid33");
				if(count($categories33)<=0)
				{
					array_push($menuarray,$parentid33);
				}
				
				$parentid=$category->parent;
				$parentcat=product_categories($parentid);
				if(count($parentcat)>0)
				{
					array_push($menuarray,$parentid);
					$tid=$parentcat[0]->term_id;
					$terms=category_term(" and term_id='$tid'");
					
					$parentid=$parentcat[0]->parent;
					$parentcat=product_categories($parentid);
					if(count($parentcat)>0)
					{
						array_push($menuarray,$parentid);
						$tid=$parentcat[0]->term_id;
						$terms=category_term(" and term_id='$tid'");
						if(count($terms)>0)
						{
							$parentid=$parentcat[0]->parent;
							$parentcat=product_categories($parentid);
							if(count($parentcat)>0)
							{
								array_push($menuarray,$parentid);
							}
						}
					}
				}
				
			}
			$menuarray2=implode(',',array_unique($menuarray));
			$categories=product_categories(''," and id in ($menuarray2) and parent='0' order by orderby");
			if(count($categories)>0)
			{
				foreach($categories as $category)
				{
					$tid=$category->term_id;
					$terms=category_term(" and term_id='$tid'");
								
					if(trim($permalink_structure)!='')
					{
						$termlink=get_option('home').'/product_category/'.$terms[0]->slug.'/?lang='.$language.'&amp;type='.$type;
					}
					else
					{
						$termlink=get_option('home').'/?product_category='.$terms[0]->slug.'&amp;lang='.$language.'&amp;type='.$type;
					}
					
					$submenu='';
					$mainclass='';
					if($terms[0]->slug=='bathroom')
					{
						$termlink='javascript:;';
						$parent=$category->id;
						
						$subcats=product_categories(''," and id in ($menuarray2) and parent='$parent' order by orderby");
						//product_categories('', " and parent='$parent' order by orderby");
						if(count($subcats)>0)
						{
							$submenu.='<ul>';
							foreach($subcats as $subcat)
							{
								$tid2=$subcat->term_id;
								$terms2=category_term(" and term_id='$tid2'");
								$termlink2='';
								if(count($terms2)>0)
								{
									if(trim($permalink_structure)!='')
									{
										$termlink2=get_option('home').'/product_category/'.$terms2[0]->slug.'/?lang='.$language.'&amp;type='.$type;
									}
									else
									{
										$termlink2=get_option('home').'/?product_category='.$terms2[0]->slug.'&amp;lang='.$language.'&amp;type='.$type;
									}
								}
								$submenu.='<li><a href="'.$termlink2.'" title="'.getText22($subcat->name,$language).'">'.getText22($subcat->name,$language).'</a></li>';
							}
							$submenu.='</ul>';
						}
					}
					$menu2.='<li><a href="'.$termlink.'">'.getText22($category->name,$language).'</a>'.$submenu.'</li>';
				}
			}
			$menu.=$menu2.'</ul>';
		}
	}
	echo $menu;
}

function othermenus2($type='')
{
	global $post, $wpdb, $q_config;
	$prefix=$wpdb->base_prefix;
	$homeLink = get_bloginfo('url');
	$language=qtrans_getLanguage();
	$permalink_structure = get_option('permalink_structure');
	
	$cond='';
	if(trim($type)!='')
	{
		$cond.=" and $type='Y'";
	}
	
	$querystr = "SELECT category_id FROM ".$prefix."products where id!='' $cond group by category_id order by category_id";
	$productcategories = $wpdb->get_results($querystr, OBJECT);
	$menu='';
	if(count($productcategories)>0)
	{
		$cats='';
		$i=1;
		foreach($productcategories as $productcategory)
		{
			if($i==1)
			{
				$cats.=$productcategory->category_id;
			}
			else
			{
				$cats.=','.$productcategory->category_id;
			}
			$i++;
		}
		$categories=product_categories(''," and id in ($cats) group by parent order by parent");
		if(count($categories)>0)
		{
			$menu.='<ul class="sub-menu">';
			$menu2='';
			$menuarray=array();
			//$j=1;
			foreach($categories as $category)
			{
				$menu3='';
				$menu4='';
				$menu5='';
				
				$parentid33=$category->id;
				
				$categories33=product_categories(''," and parent=$parentid33");
				if(count($categories33)<=0)
				{
					array_push($menuarray,$parentid33);
				}
				
				$parentid=$category->parent;
				$parentcat=product_categories($parentid);
				if(count($parentcat)>0)
				{
					array_push($menuarray,$parentid);
					$tid=$parentcat[0]->term_id;
					$terms=category_term(" and term_id='$tid'");
					
					$parentid=$parentcat[0]->parent;
					$parentcat=product_categories($parentid);
					if(count($parentcat)>0)
					{
						array_push($menuarray,$parentid);
						$tid=$parentcat[0]->term_id;
						$terms=category_term(" and term_id='$tid'");
						if(count($terms)>0)
						{
							$parentid=$parentcat[0]->parent;
							$parentcat=product_categories($parentid);
							if(count($parentcat)>0)
							{
								array_push($menuarray,$parentid);
							}
						}
					}
				}
				
			}
			$menuarray2=implode(',',array_unique($menuarray));
			$categories=product_categories(''," and id in ($menuarray2) and parent='0' order by orderby");
			if(count($categories)>0)
			{
				foreach($categories as $category)
				{
					$tid=$category->term_id;
					$terms=category_term(" and term_id='$tid'");
								
					if(trim($permalink_structure)!='')
					{
						$termlink=get_option('home').'/product_category/'.$terms[0]->slug.'/?lang='.$language.'&amp;type='.$type;
					}
					else
					{
						$termlink=get_option('home').'/?product_category='.$terms[0]->slug.'&amp;lang='.$language.'&amp;type='.$type;
					}
					
					$submenu='';
					$mainclass='';
					if($terms[0]->slug=='bathroom')
					{
						$termlink='javascript:;';
						$parent=$category->id;
						$subcats=product_categories(''," and id in ($menuarray2) and parent='$parent' order by orderby");
						//product_categories('', " and parent='$parent' order by orderby");
						if(count($subcats)>0)
						{
							$submenu.='<ul>';
							foreach($subcats as $subcat)
							{
								$tid2=$subcat->term_id;
								$terms2=category_term(" and term_id='$tid2'");
								$termlink2='';
								if(count($terms2)>0)
								{
									if(trim($permalink_structure)!='')
									{
										$termlink2=get_option('home').'/product_category/'.$terms2[0]->slug.'/?lang='.$language.'&amp;type='.$type;
									}
									else
									{
										$termlink2=get_option('home').'/?product_category='.$terms2[0]->slug.'&amp;lang='.$language.'&amp;type='.$type;
									}
								}
								$submenu.='<li><a href="'.$termlink2.'" title="'.getText22($subcat->name,$language).'">'.getText22($subcat->name,$language).'</a></li>';
							}
							$submenu.='</ul>';
						}
						$menu2.='<li><span>'.getText22($category->name,$language).'</span>'.$submenu.'</li>';
					}
					else
					{
						$menu2.='<li><a href="'.$termlink.'">'.getText22($category->name,$language).'</a>'.$submenu.'</li>';
					}
				}
			}
			$menu.=$menu2.'</ul>';
		}
	}
	echo $menu;
}

function Products_init() {
    $args = array(
		'label'  => 'Products',
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'products', 'with_front' => false ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'capability_type' => 'post',
		'show_in_nav_menus' => true,
		'exclude_from_search' => false,
		'query_var' => true,
		'can_export' => true,
		'menu_position'      => null,
		'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
    );
    register_post_type( 'products', $args );
	flush_rewrite_rules();
}
add_action( 'init', 'Products_init' );

function products_remove_menu_items() {
     remove_menu_page( 'edit.php?post_type=products' );
}
add_action( 'admin_menu', 'products_remove_menu_items' );

add_action( 'init', 'create_products_taxonomies', 0 );

// create two taxonomies, genres and writers for the post type "book"
function create_products_taxonomies() {
	// Add new taxonomy, make it hierarchical (like categories)
	$labels = array(
		'name'              => _x( 'Category', 'taxonomy general name' ),
		'singular_name'     => _x( 'Category', 'taxonomy singular name' ),
		'search_items'      => __( 'Search Category' ),
		'all_items'         => __( 'All Category' ),
		'parent_item'       => __( 'Parent Category' ),
		'parent_item_colon' => __( 'Parent Category:' ),
		'edit_item'         => __( 'Edit Category' ),
		'update_item'       => __( 'Update Category' ),
		'add_new_item'      => __( 'Add New Category' ),
		'new_item_name'     => __( 'New Category' ),
		'menu_name'         => __( 'Product Category' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'product_category' ),
	);

	register_taxonomy( 'product_category', array( 'products' ), $args );
}
function Product_install() {
   global $wpdb;
   //global $product_db_version;


$sql = "CREATE TABLE `".$wpdb->prefix."product_category` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `status` varchar(10) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT 'active',
  `description` longtext CHARACTER SET utf8 COLLATE utf8_bin,
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `add_date` datetime DEFAULT NULL,
  `parent` int(10) DEFAULT 0,
  `term_id` int(10) DEFAULT 0,
  `orderby` int(10) DEFAULT 0,
  PRIMARY KEY (`id`)
)";
$sql2 = "CREATE TABLE `".$wpdb->prefix."category_specifications` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `category_id` int(10) DEFAULT 0,
  `option_key` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `option_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `field_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `add_date` datetime DEFAULT NULL,
  `orderby` int(10) DEFAULT 0,
  `showin_filter` char(1) DEFAULT 'Y',
  PRIMARY KEY (`id`)
)";

$sql3 = "CREATE TABLE `".$wpdb->prefix."products` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `category_id` int(10) DEFAULT 0,
  `product_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `description` longtext CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `quantity` int(5) DEFAULT 0,
  `orderby` int(10) DEFAULT 0,
  `newproduct` char(1) DEFAULT 'N',
  `special` char(1) DEFAULT 'N',
  `builderoffers` char(1) DEFAULT 'N',
  `post_id` int(10) DEFAULT 0,
  `add_date` datetime DEFAULT NULL,
  `product_number` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
)";
$sql4 = "CREATE TABLE `".$wpdb->prefix."product_specifications` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `category_id` int(10) DEFAULT 0,
  `product_id` int(10) DEFAULT 0,
  `specification_id` int(10) DEFAULT 0,
  `field_key` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `field_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `field_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `add_date` datetime DEFAULT NULL,
  `orderby` int(10) DEFAULT 0,
  PRIMARY KEY (`id`)
)";
$sql5 = "CREATE TABLE `".$wpdb->prefix."product_images` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `product_id` int(10) DEFAULT 0,
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `add_date` datetime DEFAULT NULL,
  `orderby` int(10) DEFAULT 0,
  PRIMARY KEY (`id`)
)";
$sql6 = "CREATE TABLE `".$wpdb->prefix."product_attributes` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `product_id` int(10) DEFAULT 0,
  `category_id` int(10) DEFAULT 0,
  `product_number` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `colour` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `pantone_number` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `factory_number` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `stock_states1` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `stock_states2` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `orderby` int(10) DEFAULT 0,
  `add_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
)";

$sql7 = "CREATE TABLE `".$wpdb->prefix."orders` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT '0',
  `user_parent_id` int(11) DEFAULT '0',
  `order_status` char(1) DEFAULT NULL,
  `cdate` int(11) DEFAULT NULL,
  `mdate` int(11) DEFAULT NULL,
  `ip_address` varchar(15) NOT NULL DEFAULT '',
  `msg` longtext CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`order_id`)
)";

$sql8 = "CREATE TABLE `".$wpdb->prefix."order_item` (
  `order_item_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT '0',
  `user_parent_id` int(11) DEFAULT '0',
  `product_id` int(11) DEFAULT NULL,
  `attr_id` int(11) DEFAULT NULL,
  `product_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `product_number` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `colour` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `pantone_number` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `factory_number` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `stock_states1` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `stock_states2` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `product_quantity` int(10) DEFAULT '0',
  `order_status` char(1) DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `cdate` int(11) DEFAULT NULL,
  `mdate` int(11) DEFAULT NULL,
  PRIMARY KEY (`order_item_id`)
)";

$sql9 = "CREATE TABLE `".$wpdb->prefix."shop_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `setting_key` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `setting_value` longtext CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
)";

$sql10 = "CREATE TABLE `".$wpdb->prefix."users` (
  `parent_id` int(11) DEFAULT '0',
)";
   require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
   dbDelta($sql);
   dbDelta($sql2);
   dbDelta($sql3);
   dbDelta($sql4);
   dbDelta($sql5);
   dbDelta($sql6);
   dbDelta($sql7);
   dbDelta($sql8);
   dbDelta($sql9);
   dbDelta($sql10);
}
register_activation_hook(__FILE__,'Product_install');
?>